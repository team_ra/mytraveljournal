-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mag 02, 2020 alle 17:29
-- Versione del server: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_mytravelj`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `attractions`
--

CREATE TABLE IF NOT EXISTS `attractions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `trips`
--

CREATE TABLE IF NOT EXISTS `trips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departureDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `status` enum('done','organized') NOT NULL,
  `notes` varchar(300),
  `title` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userId`) REFERENCES users(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(100) NOT NULL,
  `image` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `visitedCities`
--

CREATE TABLE IF NOT EXISTS `visitedCities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityId` int(11) NOT NULL,
  `tripId` int(11) NOT NULL,
  `preference` int(1) DEFAULT NULL,
  `visitedDate` date DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`cityId`) REFERENCES cities(`id`),
  FOREIGN KEY (`tripId`) REFERENCES trips(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `visistedAttractions`
--

CREATE TABLE IF NOT EXISTS `visitedAttractions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attractionId` int(11) NOT NULL,
  `tripId` int(11) NOT NULL,
  `preference` int(1) DEFAULT NULL,
  `visitedDate` date DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`attractionId`) REFERENCES attractions(`id`),
  FOREIGN KEY (`tripId`) REFERENCES trips(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `imagesCities`
--
CREATE TABLE IF NOT EXISTS `imagesCities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visitedCityId` int(11) NOT NULL,
  `image` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`visitedCityId`) REFERENCES visitedCities(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `imagesAttractions`
--
CREATE TABLE IF NOT EXISTS `imagesAttractions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visitedAttractionId` int(11) NOT NULL,
  `image` mediumtext,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`visitedAttractionId`) REFERENCES visitedAttractions(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `desiredCities`
--

CREATE TABLE IF NOT EXISTS `desiredCities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`cityId`) REFERENCES cities(`id`),
  FOREIGN KEY (`userId`) REFERENCES users(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `desiredCities`
--

CREATE TABLE IF NOT EXISTS `desiredAttractions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attractionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`attractionId`) REFERENCES attractions(`id`),
  FOREIGN KEY (`userId`) REFERENCES users(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
