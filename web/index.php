<?php

    define("OUTPUT_ERROR_MSG", "Errore inaspettato, riprova.");
    $output = array("result" => "error", "message" => OUTPUT_ERROR_MSG);

    $_db = @mysqli_connect("localhost", "mytravelj", "", "my_mytravelj")or die("<center style='padding-top:50px;font-family:sans-serif;font-size:18px;'>Error during database connection :(</center>");
    mysqli_set_charset($_db, "utf8");

    function output(){
        global $output;
        header("Content-type: application/json");
        exit(json_encode($output));
    }

    function escape($string){
        global $_db;
        return mysqli_real_escape_string($_db, $string);
    }

    function password($password){
        // Salt + Email before hash
        return md5("[#MYTRAVELJOURNAL-SALT-LEFT#]".$password."[#MYTRAVELJOURNAL-SALT-RIGHT#]");
    }
    
    if(isset($_POST["type"])){
    	switch ($_POST["type"]){
        	case 'registration':
                if(!isset($_POST["username"]) || !isset($_POST["password"]) 
                   || !isset($_POST["email"]) || !isset($_POST["image"])
                   || trim($_POST["username"])=="" || trim($_POST["password"]=="")
                   || trim($_POST["email"])=="" || trim($_POST["image"])==""){
                    $output["message"]="Non hai compilato tutti i campi obbligatori";
                }else{
                    if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                        $output["message"]="L'indirizzo email inserito non è valido";
                    }else{
                        if(mysqli_num_rows(mysqli_query($_db, "SELECT null FROM users WHERE email='".escape($_POST["email"])."'"))!=0){
                            $output["message"]="Esiste già un utente con questo indirizzo email";
                        }else{
                            mysqli_query($_db, "INSERT INTO users (email, password, username, image) VALUES ('".escape($_POST["email"])."', '".escape(password($_POST["password"]))."', '".escape($_POST["username"])."', '".escape($_POST["image"])."')");
                            $output["result"]="success";
                            $output["message"]="Registrazione eseguita con successo!";
                        }
                    }
                }
                break;
            case 'login':
                if(!isset($_POST["email"]) || !isset($_POST["password"]) 
                    || trim($_POST["email"])=="" || trim($_POST["password"])==""){
                    $output["message"]="E-mail o password non inseriti!";
                }else{
                    $data=mysqli_query($_db, "SELECT username,
                                                         email,
                                                         image 
                                                  FROM users 
                                                  WHERE email='".escape($_POST["email"])."' 
                                                        and password='".escape(password($_POST["password"]))."'");
                    if(mysqli_num_rows($data)!=0){
                        $data=mysqli_fetch_assoc($data);
                        $output["username"]=$data["username"];
                        $output["email"]=$data["email"];
                        $output["image"]=$data["image"];
                        $output["result"]="success";
                        $output["message"]="Login eseguito con successo con successo!";
                    }else{
                        $output["message"]="E-mail o password non corretti!";
                    }  
                }
                break;
            case 'addPlannedTrip':
                if(!isset($_POST["title"]) || !isset($_POST["departureDate"]) || 
                   !isset($_POST["returnDate"]) || !isset($_POST["email"])){
                    $output["message"]="Non hai inserito tutti i dati!";
                }else{
                    $data = mysqli_query($_db, "SELECT id
                                                FROM users 
                                                WHERE email='".escape($_POST["email"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"]="L'utente inserito è inesistente";
                    }else{
                        $userid=mysqli_fetch_assoc($data)["id"];
                        $departureDate=date('Y-m-d',strtotime($_POST["departureDate"]));
                        $returnDate=date('Y-m-d',strtotime($_POST["returnDate"]));
                        $query = "SELECT id
                                  FROM trips 
                                  WHERE title='".escape($_POST["title"])."'
                                        and departureDate='$departureDate'
                                        and userId=$userid
                                        and returnDate='$returnDate'";
                        if(mysqli_num_rows(mysqli_query($_db, $query))!=0){
                            $output["message"]="Hai già salvato un viaggio con le stesse caratteristiche";
                        }else{
                            mysqli_query($_db, "INSERT INTO trips(title, departureDate, returnDate, status, userId) VALUES ('".escape($_POST["title"])."', '$departureDate', '$returnDate', 'organized', $userid)");
                            $data=mysqli_query($_db, $query);
                            $tripId=mysqli_fetch_assoc($data)["id"];
                            if(isset($_POST["cities"])){
                            	$cities=ltrim($_POST["cities"],"[");
        						$cities=chop($cities,"]");
        						$citiesInThisTrip=explode(",",$cities);
                                for($i=0; $i<count($citiesInThisTrip); $i++){
                                    $city = trim($citiesInThisTrip[$i]);
                                    $query = "SELECT id
                                              FROM cities 
                                              WHERE name='$city'";
                                    if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                        mysqli_query($_db, "INSERT INTO cities(name) VALUES ('$city')");
                                    }
                                    $data = mysqli_query($_db, $query);
                                    $idCity=mysqli_fetch_assoc($data)["id"];
                                    mysqli_query($_db, "INSERT INTO visitedCities(tripId, cityId) VALUES ($tripId, $idCity)");
                                }
                            }
                            if(isset($_POST["attractions"])){
                            	$attractions=ltrim($_POST["attractions"],"[");
        						$attractions=chop($attractions,"]");
        						$attractionsInThisTrip=explode(",",$attractions);
                                for($i=0; $i<count($attractionsInThisTrip); $i++){
                                    $attraction = trim($attractionsInThisTrip[$i]);
                                    $query = "SELECT id
                                              FROM attractions 
                                              WHERE name='$attraction'";
                                    if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                        mysqli_query($_db, "INSERT INTO attractions(name) VALUES ('$attraction')");
                                    }
                                    $data=mysqli_query($_db, $query);
                                    $idAttraction=mysqli_fetch_assoc($data)["id"];
                                    mysqli_query($_db, "INSERT INTO visitedAttractions(tripId, attractionId) VALUES ($tripId, $idAttraction)");
                                }
                            }
                            $output["result"]="success";
                            $output["message"]="Viaggio aggiunto correttamente";
                        }
                    }
                }
                break;
            case 'getPlannedTrips':
                if(!isset($_POST["email"])){
                    $output["message"]="Non esiste un utente con tale email";
                }else{
                    $data = mysqli_query($_db, "SELECT t.id,
                                                       t.title,
                                                       t.departureDate,
                                                       t.returnDate
                                                    FROM trips t, 
                                                         users u 
                                                    WHERE u.email='".escape($_POST["email"])."'
                                                          and u.id = t.userId
                                                          and t.status='organized'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Non hai ancora organizzato alcun viaggio!";
                    }else{
                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        $output["values"] = JSON_encode($data);
                        $output["result"] = "success";
                    }
                }
            break;
            //cancella un viaggio programmato
            case 'deletePlannedTrip':
                //controllo se la richiesta contiene tutti i campi necessari
                if( !isset($_POST["title"]) || !isset($_POST["departureDate"]) || !isset($_POST["returnDate"]) || !isset($_POST["email"])){
                    $output["message"]="Non sono stati inseriti tutti i campi";
                }else{
                     //converte stringhe in date
                    $departureDate=date('Y-m-d',strtotime($_POST["departureDate"]));
                    $returnDate=date('Y-m-d',strtotime($_POST["returnDate"]));
                    $data = mysqli_query($_db, "SELECT t.id
                                                    FROM trips t, 
                                                         users u 
                                                    WHERE u.email='".escape($_POST["email"])."'
                                                          and u.id = t.userId
                                                          and t.status='organized'
                                                          and t.title='".escape($_POST["title"])."'
                                                          and t.departureDate='$departureDate'
                                                          and t.returnDate='$returnDate'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Il viaggio che stai cercando non è mai stato organizzato!";
                    }else{
                        //una volta che ho trovato il viaggio lo cancello
                        $idTrip=mysqli_fetch_assoc($data)["id"];
                        mysqli_query($_db, "DELETE FROM visitedCities WHERE tripId='$idTrip'");
                		mysqli_query($_db, "DELETE FROM visitedAttractions WHERE tripId='$idTrip'");
                        mysqli_query($_db, "DELETE FROM trips WHERE id='$idTrip'");
                        $output["result"] = "success";
                        $output["message"] = "viaggio eliminato con successo!";
                    }
                }
            break;
            case 'addDoneTrip':
                //controllo se la richiesta contiene tutti i campi necessari
                if( !isset($_POST["title"]) || !isset($_POST["departureDate"]) || !isset($_POST["returnDate"]) || !isset($_POST["email"])){
                    $output["message"]="Non sono stati inseriti tutti i campi";
                }else{
                	//converte stringhe in date
                	$departureDate = date('Y-m-d',strtotime($_POST["departureDate"]));
                	$returnDate = date('Y-m-d',strtotime($_POST["returnDate"]));
                    $data = mysqli_query($_db, "SELECT t.id
                                                    FROM trips t, 
                                                         users u 
                                                    WHERE u.email='".escape($_POST["email"])."'
                                                          and u.id = t.userId
                                                          and t.status='organized'
                                                          and t.title='".escape($_POST["title"])."'
                                                          and t.departureDate='$departureDate'
                                                          and t.returnDate='$returnDate'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Il viaggio che stai cercando non è mai stato organizzato!";
                    }else{
                        //una volta che ho trovato il viaggio lo aggiungo a viaggi effettuati
                        $idTrip=mysqli_fetch_assoc($data)["id"];
                        mysqli_query($_db, "UPDATE trips SET status='done' WHERE id='$idTrip'");
                        $output["result"] = "success";
                    }
                }
            break;
            case 'getPlannedTrip':
                if(!isset($_POST["tripId"])){
                    $output["message"]="Non sono stati inseriti tutti i campi obbligatori!";
                }else{
                    $data = mysqli_query($_db, "SELECT null
                                                FROM trips t
                                                WHERE t.id='".escape($_POST["tripId"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Non hai mai organizzato un viaggio con tale caratteristiche!";
                    }else{
                        //cerco se ci sono città
                        $data = mysqli_query($_db, "SELECT c.name
                                                    FROM visitedCities v, 
                                                         cities c
                                                WHERE c.id=v.cityId
                                                      and v.tripId='".escape($_POST["tripId"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["cities"] = "nessuna città";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["cities"] = JSON_encode($data);
                        }
                        //cerco se ci sono attrazioni
                        $data = mysqli_query($_db, "SELECT a.name
                                                FROM visitedAttractions v, 
                                                     attractions a
                                                WHERE a.id=v.attractionId
                                                      and v.tripId='".escape($_POST["tripId"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["attractions"] = "nessuna attrazione";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["attractions"] = JSON_encode($data);
                        }
                        $output["result"] = "success";
                    }
                }
            break;
            case 'updateDate':
                if(!isset($_POST["tripId"]) && (!isset($_POST["departureDate"]) || !isset($_POST["returnDate"]))){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                     $data = mysqli_query($_db, "SELECT null
                                                FROM trips
                                                WHERE id='".escape($_POST["tripId"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Il viaggio inserito non esiste!";
                    }else{
                        if(isset($_POST["departureDate"])){
                            $departureDate=date('Y-m-d',strtotime($_POST["departureDate"]));
                            mysqli_query($_db, "UPDATE trips SET departureDate='$departureDate' WHERE id='".escape($_POST["tripId"])."'");
                        }else{
                            $returnDate=date('Y-m-d',strtotime($_POST["returnDate"]));
                            mysqli_query($_db, "UPDATE trips SET returnDate='$returnDate' WHERE id='".escape($_POST["tripId"])."'");
                        }
                        $output["result"] = "success";
                    }
                }
            break;
            case 'updateTrip':
                if(!isset($_POST["tripId"]) && (!isset($_POST["cities"]) || !isset($_POST["attractions"]))){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                     $data = mysqli_query($_db, "SELECT null
                                                FROM trips
                                                WHERE id='".escape($_POST["tripId"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Il viaggio inserito non esiste!";
                    }else{
                        if(isset($_POST["cities"])){
                            $cities=ltrim($_POST["cities"],"[");
                            $cities=chop($cities,"]");
                            $citiesInThisTrip=explode(",",$cities);
                            for($i=0; $i<count($citiesInThisTrip); $i++){
                                $citiesInThisTrip[$i] = trim($citiesInThisTrip[$i]);
                            }
                            for($i=0; $i<count($citiesInThisTrip); $i++){
                                $city = trim($citiesInThisTrip[$i]);
                                $query = "SELECT id
                                        FROM cities 
                                        WHERE name='$city'";
                                if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                    mysqli_query($_db, "INSERT INTO cities(name) VALUES ('$city')");
                                }
                                $data = mysqli_query($_db, $query);
                                $idCity=mysqli_fetch_assoc($data)["id"];
                                $query = "SELECT id
                                            FROM visitedCities 
                                            WHERE tripId='".escape($_POST["tripId"])."'
                                                and cityId=$idCity";
                                if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                    mysqli_query($_db, "INSERT INTO visitedCities(tripId, cityId) VALUES ('".escape($_POST["tripId"])."', $idCity)");
                                }
                            }
                        }
                        if(isset($_POST["attractions"])){
                            $attractions=ltrim($_POST["attractions"],"[");
                            $attractions=chop($attractions,"]");
                            $attractionsInThisTrip=explode(",",$attractions);
                            for($i=0; $i<count($attractionsInThisTrip); $i++){
                                $attractionsInThisTrip[$i] = trim($attractionsInThisTrip[$i]);
                            }
                            for($i=0; $i<count($attractionsInThisTrip); $i++){
                                $attraction = trim($attractionsInThisTrip[$i]);
                                $query = "SELECT id
                                        FROM attractions 
                                        WHERE name='$attraction'";
                                if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                    mysqli_query($_db, "INSERT INTO attractions(name) VALUES ('$attraction')");
                                }
                                $data = mysqli_query($_db, $query);
                                $attractionId = mysqli_fetch_assoc($data)["id"];
                                $query = "SELECT id
                                            FROM visitedAttractions 
                                            WHERE tripId='".escape($_POST["tripId"])."'
                                                and attractionId=$attractionId";
                                if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                    mysqli_query($_db, "INSERT INTO visitedAttractions(tripId, attractionId) VALUES ('".escape($_POST["tripId"])."', $attractionId)");
                                }
                            }
                        }
                        $output["result"] = "success";
                    }
                }
            break;
            case 'deletePlace':
                if(!isset($_POST["tripId"]) || !isset($_POST["kind"]) || !isset($_POST["name"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    if($_POST["kind"] = "city"){
                        $data = mysqli_query($_db, "SELECT vc.id
                                                    FROM visitedCities vc,
                                                         cities c
                                                    WHERE vc.cityId = c.id and 
                                                          vc.tripId='".escape($_POST["tripId"])."' and
                                                          c.name='".escape($_POST["name"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Il viaggio inserito non esiste!";
                        }else{
                            $visitedCityId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM visitedCities WHERE id=$visitedCityId");
                        }
                    }
                    if($_POST["kind"] = "attraction"){
                        $data = mysqli_query($_db, "SELECT va.id
                                                    FROM visitedAttractions va,
                                                         attractions a
                                                    WHERE va.attractionId = a.id and 
                                                          va.tripId='".escape($_POST["tripId"])."' and 
                                                          a.name='".escape($_POST["name"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Il viaggio inserito non esiste!";
                        }else{
                            $visitedAttractionId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM visitedAttractions WHERE id=$visitedAttractionId");
                        }
                    }
                    $output["result"] = "success";
                }
            break;
            case 'getPlaceImages':
                if(!isset($_POST["tripId"]) || !isset($_POST["kind"]) || !isset($_POST["name"]) || !isset($_POST["tripId"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    if($_POST["kind"] != "attraction"){
                        $data = mysqli_query($_db, "SELECT image
                                                        FROM visitedCities v, 
                                                             imagesCities i,
                                                             cities c
                                                        WHERE c.id = v.cityId
                                                            and v.id = i.visitedCityId
                                                            and v.tripId='".escape($_POST["tripId"])."'
                                                            and c.name='".escape($_POST["name"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["images"] = "nessuna immagine";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["images"] = JSON_encode($data);                           
                        }
                        $output["result"] = "success"; 
                    }else{
                        $data = mysqli_query($_db, "SELECT image
                                                        FROM visitedAttractions v, 
                                                             imagesAttractions i,
                                                             attractions a
                                                        WHERE a.id = v.attractionId
                                                            and v.id = i.visitedAttractionId
                                                            and v.tripId='".escape($_POST["tripId"])."'
                                                            and a.name='".escape($_POST["name"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["images"] = "nessuna immagine";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["images"] = JSON_encode($data);                              
                        }
                        $output["result"] = "success"; 
                    }
                }
            break;
            case 'addPlaceImage':
                if(!isset($_POST["tripId"]) || !isset($_POST["kind"]) || !isset($_POST["name"]) || !isset($_POST["image"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    if($_POST["kind"] != "attraction"){
                        $data = mysqli_query($_db, "SELECT v.id
                                                        FROM visitedCities v, 
                                                             cities c
                                                        WHERE c.id = v.cityId
                                                            and v.tripId='".escape($_POST["tripId"])."'
                                                            and c.name='".escape($_POST["name"])."'");
                        $visitedCityId = mysqli_fetch_assoc($data)["id"];
                        mysqli_query($_db, "INSERT INTO imagesCities(visitedCityId, image) VALUES ($visitedCityId, '".escape($_POST["image"])."')");
                        $output["result"] = "success"; 
                    }else{
                        $data = mysqli_query($_db, "SELECT v.id
                                                    FROM visitedAttractions v,
                                                            attractions a
                                                    WHERE a.id = v.attractionId
                                                        and v.tripId='".escape($_POST["tripId"])."'
                                                        and a.name='".escape($_POST["name"])."'");
                        $visitedAttractionId = mysqli_fetch_assoc($data)["id"];
                        mysqli_query($_db, "INSERT INTO imagesAttractions(visitedAttractionId, image) VALUES ($visitedAttractionId, '".escape($_POST["image"])."')");
                        $output["result"] = "success";
                    }
                }
            break;
            case 'deletePlaceImage':
                if(!isset($_POST["tripId"]) || !isset($_POST["kind"]) || !isset($_POST["name"]) || !isset($_POST["image"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    if($_POST["kind"] != "attraction"){
                        $data = mysqli_query($_db, "SELECT i.id
                                                        FROM visitedCities v, 
                                                             imagesCities i,
                                                             cities c
                                                        WHERE c.id = v.cityId
                                                            and v.id = i.visitedCityId
                                                            and v.tripId='".escape($_POST["tripId"])."'
                                                            and c.name='".escape($_POST["name"])."'
                                                            and i.image='".escape($_POST["image"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non esiste tale immagine!";
                        }else{
                            $imageId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM imagesCities WHERE id='$imageId'");
                            $output["result"] = "success";                            
                        }
                    }else{
                        $data = mysqli_query($_db, "SELECT i.id
                                                    FROM visitedAttractions v, 
                                                         imagesAttractions i,
                                                         attractions a
                                                    WHERE a.id = v.attractionId
                                                        and v.id = i.visitedAttractionId
                                                        and v.tripId='".escape($_POST["tripId"])."'
                                                        and a.name='".escape($_POST["name"])."'
                                                        and i.image='".escape($_POST["image"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non esiste tale immagine!";
                        }else{
                            $imageId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM imagesAttractions WHERE id='$imageId'");
                            $output["result"] = "success";                              
                        }
                    }
                }
            break;
            case 'getDesiredPlaces':
                if(!isset($_POST["email"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    //cerco se ci sono città
                    $data = mysqli_query($_db, "SELECT c.name
                                                FROM desiredCities d, 
                                                     cities c,
                                                     users u
                                                WHERE c.id=d.cityId
                                                    and u.id=d.userId
                                                    and u.email='".escape($_POST["email"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["cities"] = "nessuna città";
                    }else{
                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        $output["cities"] = JSON_encode($data);
                    }
                    //cerco se ci sono attrazioni
                    $data = mysqli_query($_db, "SELECT a.name
                                                FROM desiredAttractions d, 
                                                     attractions a,
                                                     users u
                                                WHERE a.id=d.attractionId
                                                    and u.id=d.userId
                                                    and u.email='".escape($_POST["email"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["attractions"] = "nessuna attrazione";
                    }else{
                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        $output["attractions"] = JSON_encode($data);
                    }
                    $output["result"] = "success";
                }
            break;
            case 'addDesiredPlace':
                if(!isset($_POST["kind"]) || !isset($_POST["email"]) || !isset($_POST["name"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    $data = mysqli_query($_db, "SELECT id
                                                FROM users 
                                                WHERE email='".escape($_POST["email"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"]="L'utente inserito è inesistente";
                    }else{
                        $userId=mysqli_fetch_assoc($data)["id"];
                        if($_POST["kind"] == "city"){
                            $query = "SELECT id
                                    FROM cities 
                                    WHERE name='".escape($_POST["name"])."'";
                            if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                mysqli_query($_db, "INSERT INTO cities(name) VALUES ('".escape($_POST["name"])."')");
                            }
                            $data = mysqli_query($_db, $query);
                            $cityId=mysqli_fetch_assoc($data)["id"];
                            $query = "SELECT null
                                        FROM desiredCities v 
                                        WHERE userId=$userId
                                        and cityId=$cityId";
                            if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                mysqli_query($_db, "INSERT INTO desiredCities(userId, cityId) VALUES ($userId, $cityId)");
                                $output["result"] = "success";
                            }else{
                            	$output["message"] = "Città già inserita!";
                            }
                        }else{
                            $query = "SELECT id
                                    FROM attractions 
                                    WHERE name='".escape($_POST["name"])."'";
                            if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                mysqli_query($_db, "INSERT INTO attractions(name) VALUES ('".escape($_POST["name"])."')");
                            }
                            $data = mysqli_query($_db, $query);
                            $attractionId=mysqli_fetch_assoc($data)["id"];
                            $query = "SELECT null
                                        FROM desiredAttractions v 
                                        WHERE userId=$userId
                                        and attractionId=$attractionId";
                            if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                mysqli_query($_db, "INSERT INTO desiredAttractions(userId, attractionId) VALUES ($userId, $attractionId)");
                                $output["result"] = "success";
                            }else{
                            	$output["message"] = "Attrazione già inserita!";
                            }
                        }
                    }
                }
            break;
            case 'deleteDesiredPlace':
                if(!isset($_POST["email"]) || !isset($_POST["kind"]) || !isset($_POST["name"])){
                    $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                }else{
                    if($_POST["kind"] != "attraction"){
                        $data = mysqli_query($_db, "SELECT d.id
                                                    FROM desiredCities d, 
                                                            cities c,
                                                            users u
                                                    WHERE c.id = d.cityId
                                                        and u.id= d.userId
                                                        and c.name='".escape($_POST["name"])."'
                                                        and u.email='".escape($_POST["email"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non esiste tale città!";
                        }else{
                            $desiredCityId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM desiredCities WHERE id='$desiredCityId'");
                            $output["result"] = "success";                            
                        }
                    }else{
                        $data = mysqli_query($_db, "SELECT d.id
                                                    FROM desiredAttractions d, 
                                                         users u,
                                                         attractions a
                                                    WHERE a.id = d.attractionId
                                                        and u.id = d.userId
                                                        and a.name='".escape($_POST["name"])."'
                                                        and u.email='".escape($_POST["email"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non esiste tale attrazione!";
                        }else{
                            $desiredAttractionId = mysqli_fetch_assoc($data)["id"];
                            mysqli_query($_db, "DELETE FROM desiredAttractions WHERE id='$desiredAttractionId'");
                            $output["result"] = "success";                              
                        }
                    }
                }
            break;
            case 'getRandomTrips':
                if(!isset($_POST["email"])){
                    $output["messagge"] = "Non è presente alcun viaggio!";
                }else{
                    $data = mysqli_query($_db, "SELECT t.title,
                                                        t.departureDate,
                                                        t.returnDate,
                                                        t.id
                                                FROM trips t, 
                                                     users u 
                                                WHERE u.email='".escape($_POST["email"])."'
                                                        and u.id = t.userId
                                                        and t.status='done'
                                                ORDER BY RAND() 
                                                LIMIT 3");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Non hai ancora organizzato alcun viaggio!";
                    }else{
                        $values = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        $output["values"] = JSON_encode($values);
                        for($i=0; $i<count($values); $i++){
                            $val = $values[$i]["id"];
                            $data = mysqli_query($_db, "SELECT ic.image as image
                                                        FROM visitedCities vc,
                                                             imagesCities ic
                                                        WHERE vc.id = ic.visitedCityId
                                                             and vc.tripId = $val
                                                        ORDER BY RAND() 
                                                        LIMIT 1");
                             if(mysqli_num_rows($data)!=0){
                                $images[$i] = mysqli_fetch_assoc($data)["image"];
                             }else{
                                $data = mysqli_query($_db, "SELECT ic.image as image
                                                            FROM visitedAttractions vc,
                                                                imagesAttractions ic
                                                            WHERE vc.id = ic.visitedAttractionId
                                                                and vc.tripId = $val
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                                if(mysqli_num_rows($data)!=0){
                                    $images[$i] = mysqli_fetch_assoc($data)["image"];
                                }else{
                                    $images[$i] = "nessuna immagine";
                                }
                             }
                        }
                        $output["images"] = JSON_encode($images);
                        $output["result"] = "success";
                    }
                }
                break;
                case 'getDoneTrips':
                    if(!isset($_POST["email"])){
                        $output["messagge"] = "Non è presente alcun viaggio!";
                    }else{
                        $data = mysqli_query($_db, "SELECT t.title,
                                                            t.departureDate,
                                                            t.returnDate,
                                                            t.id
                                                    FROM trips t, 
                                                         users u 
                                                    WHERE u.email='".escape($_POST["email"])."'
                                                            and u.id = t.userId
                                                            and t.status='done'
                                                    ORDER BY t.departureDate DESC");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non hai ancora organizzato alcun viaggio!";
                        }else{
                            $values = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["values"] = JSON_encode($values);
                            for($i=0; $i<count($values); $i++){
                                $val = $values[$i]["id"];
                                $data = mysqli_query($_db, "SELECT ic.image as image
                                                            FROM visitedCities vc,
                                                                 imagesCities ic
                                                            WHERE vc.id = ic.visitedCityId
                                                                 and vc.tripId = $val
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                                 if(mysqli_num_rows($data)!=0){
                                    $images[$i] = mysqli_fetch_assoc($data)["image"];
                                 }else{
                                    $data = mysqli_query($_db, "SELECT ic.image as image
                                                                FROM visitedAttractions vc,
                                                                    imagesAttractions ic
                                                                WHERE vc.id = ic.visitedAttractionId
                                                                    and vc.tripId = $val
                                                                ORDER BY RAND() 
                                                                LIMIT 1");
                                    if(mysqli_num_rows($data)!=0){
                                        $images[$i] = mysqli_fetch_assoc($data)["image"];
                                    }else{
                                        $images[$i] = "nessuna immagine";
                                    }
                                 }
                            }
                            $output["images"] = JSON_encode($images);
                            $output["result"] = "success";
                        }
                    }
                    break;
          		case 'updateUserData':
                    if(!isset($_POST["oldEmail"])){
                        $output["message"]="Non hai compilato tutti i campi obbligatori";
                    }else{
                        $data = mysqli_query($_db, "SELECT id
                                                    FROM users 
                                                    WHERE email='".escape($_POST["oldEmail"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"]="NON esiste alcun utente con tale indirizzo email";
                        }else{
                            $userId = mysqli_fetch_assoc($data)["id"];
                            if(isset($_POST["email"])){
                                if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                                    $output["message"]="L'indirizzo email inserito non è valido";
                                }else{
                                	$data = mysqli_query($_db, "SELECT null
                                                    FROM users 
                                                    WHERE email='".escape($_POST["email"])."'");
                                    if(mysqli_num_rows($data)==0){
                                    	mysqli_query($_db, "UPDATE users SET email='".escape($_POST["email"])."' WHERE id='$userId'");
                                        if(isset($_POST["username"])){
                                            mysqli_query($_db, "UPDATE users SET username='".escape($_POST["username"])."' WHERE id='$userId'");
                                            $output["result"]="success";
                                        }
                                        if(isset($_POST["password"])){
                                            mysqli_query($_db, "UPDATE users SET password='".escape($_POST["password"])."' WHERE id='$userId'");
                                            $output["result"]="success";
                                        }
                                        if(isset($_POST["image"])){
                                            mysqli_query($_db, "UPDATE users SET image='".escape($_POST["image"])."' WHERE id='$userId'");
                                            $output["result"]="success";
                                        }
                                    }else{
                                    	$output["message"]="L'indirizzo email è già utilizzato da un altro utente";
                                    }
                                }
                            }else{
                                if(isset($_POST["username"])){
                                    mysqli_query($_db, "UPDATE users SET username='".escape($_POST["username"])."' WHERE id='$userId'");
                                    $output["result"]="success";
                                }
                                if(isset($_POST["password"])){
                                    mysqli_query($_db, "UPDATE users SET password='".escape($_POST["password"])."' WHERE id='$userId'");
                                    $output["result"]="success";
                                }
                                if(isset($_POST["image"])){
                                    mysqli_query($_db, "UPDATE users SET image='".escape($_POST["image"])."' WHERE id='$userId'");
                                    $output["result"]="success";
                                }
                            }
                            $data = mysqli_query($_db, "SELECT username, 
                                                                email,
                                                                image
                                                        FROM users 
                                                        WHERE id='$userId'");
                            $data = mysqli_fetch_assoc($data);
                            $output["email"] = $data["email"];
                            $output["username"] = $data["username"];
                        }
                    }
                    break;
                case 'getPlaceDates':
                    if(!isset($_POST["tripId"])){
                        $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                    }else{
                        //cerco se ci sono città
                        $data = mysqli_query($_db, "SELECT c.name,
                                                            v.visitedDate,
                                                            v.id
                                                    FROM visitedCities v, 
                                                            cities c
                                                    WHERE c.id=v.cityId
                                                        and v.tripId='".escape($_POST["tripId"])."'
                                                    ORDER BY v.visitedDate ASC");
                        if(mysqli_num_rows($data)==0){
                            $output["cities"] = "nessuna città";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["cities"] = JSON_encode($data);
                        }
                        //cerco se ci sono attrazioni
                        $data = mysqli_query($_db, "SELECT a.name,
                                                            v.visitedDate,
                                                            v.id
                                                    FROM visitedAttractions v, 
                                                            attractions a
                                                    WHERE a.id=v.attractionId
                                                        and v.tripId='".escape($_POST["tripId"])."'
                                                    ORDER BY v.visitedDate ASC");
                        if(mysqli_num_rows($data)==0){
                            $output["attractions"] = "nessuna attrazione";
                        }else{
                            $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                            $output["attractions"] = JSON_encode($data);
                        }
                        $output["result"] = "success";
                    }
                    break;
                case 'getDoneTrip':
                    if(!isset($_POST["id"])){
                        $output["message"]="Non sono stati inseriti tutti i campi obbligatori!";
                    }else{
                        $data = mysqli_query($_db, "SELECT t.id
                                                        FROM trips t
                                                        WHERE t.id='".escape($_POST["id"])."'");
                        if(mysqli_num_rows($data)==0){
                            $output["message"] = "Non hai mai organizzato un viaggio con tale caratteristiche!";
                        }else{
                            //cerco se ci sono città
                            $data = mysqli_query($_db, "SELECT c.name
                                                        FROM visitedCities v, 
                                                                cities c
                                                    WHERE c.id=v.cityId
                                                            and v.tripId='".escape($_POST["id"])."'");
                            if(mysqli_num_rows($data)==0){
                                $output["cities"] = "nessuna città";
                                $output["citiesImages"] = "nessuna immagine";
                            }else{
                                $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                $output["cities"] = JSON_encode($data);
                                $data = mysqli_query($_db, "SELECT ic.image as image
                                                        FROM visitedCities vc,
                                                                imagesCities ic
                                                        WHERE vc.id = ic.visitedCityId
                                                                and vc.tripId ='".escape($_POST["id"])."'
                                                        ORDER BY RAND() 
                                                		LIMIT 2");
                                if(mysqli_num_rows($data)==0){
                                    $output["citiesImages"] = "nessuna immagine";
                                }else{
                                    $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                    $output["citiesImages"] = JSON_encode($data);
                                }
                            }
                            //cerco se ci sono attrazioni
                            $data = mysqli_query($_db, "SELECT a.name
                                                    FROM visitedAttractions v, 
                                                            attractions a
                                                    WHERE a.id=v.attractionId
                                                            and v.tripId='".escape($_POST["id"])."'");
                            if(mysqli_num_rows($data)==0){
                                $output["attractions"] = "nessuna attrazione";
                                $output["attractionsImages"] = "nessuna immagine";
                            }else{
                                $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                $output["attractions"] = JSON_encode($data);
                                $data = mysqli_query($_db, "SELECT ic.image as image
                                                            FROM visitedAttractions vc,
                                                                imagesAttractions ic
                                                            WHERE vc.id = ic.visitedAttractionId
                                                                and vc.tripId = '".escape($_POST["id"])."' 
                                                          ORDER BY RAND() 
                                                          LIMIT 2");
                                if(mysqli_num_rows($data)==0){
                                    $output["attractionsImages"] = "nessuna immagine";
                                }else{ 
                                    $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                    $output["attractionsImages"] = JSON_encode($data);
                                }
                            }
                            $output["result"] = "success";
                        }
                    }
                    break; 
                    case 'getDonePlace':
                        if(!isset($_POST["email"]) || !isset($_POST["title"]) || !isset($_POST["departureDate"])
                            || !isset($_POST["returnDate"]) || !isset($_POST["kind"]) || !isset($_POST["name"])){
                            $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                        }else{
                            $departureDate=date('Y-m-d',strtotime($_POST["departureDate"]));
                            $returnDate=date('Y-m-d',strtotime($_POST["returnDate"]));
                            if($_POST["kind"]=="city"){
                                $data = mysqli_query($_db, "SELECT v.visitedDate,
                                                                v.notes,
                                                                v.preference,
                                                                v.id
                                                            FROM visitedCities v, 
                                                                cities c,
                                                                users u,
                                                                trips t
                                                            WHERE c.id=v.cityId
                                                                and t.id=v.tripId
                                                                and u.id=t.userId
                                                                and u.email='".escape($_POST["email"])."'
                                                                and t.title='".escape($_POST["title"])."'
                                                                and t.departureDate='$departureDate'
                                                                and t.returnDate='$returnDate'
                                                                and c.name='".escape($_POST["name"])."'");
                                if(mysqli_num_rows($data)==0){
                                    $output["message"] = "non esiste tale città";
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $output["place"] = $data;
                                    $visitedCityId = $data["id"];
                                    $data = mysqli_query($_db, "SELECT ic.image as image
                                                                FROM imagesCities ic
                                                                WHERE ic.visitedCityId = $visitedCityId");
                                    if(mysqli_num_rows($data)==0){
                                        $output["images"] = "nessuna immagine";
                                    }else{
                                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                        $output["images"] = JSON_encode($data);
                                    }
                                    $output["result"] = "success";
                                }
                            }else{
                                //cerco se ci sono attrazioni
                                $data = mysqli_query($_db, "SELECT v.visitedDate,
                                                                    v.notes,
                                                                    v.preference,
                                                                    v.id
                                                            FROM visitedAttractions v, 
                                                                 attractions a, 
                                                                 users u,
                                                                 trips t
                                                            WHERE a.id=v.attractionId
                                                                and t.id=v.tripId
                                                                and u.id=t.userId
                                                                and u.email='".escape($_POST["email"])."'
                                                                and t.title='".escape($_POST["title"])."'
                                                                and t.departureDate='$departureDate'
                                                                and t.returnDate='$returnDate'
                                                                and a.name='".escape($_POST["name"])."'");
                                if(mysqli_num_rows($data)==0){
                                    $output["attractions"] = "non esiste tale attrazione";
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $output["place"] = $data;
                                    $visitedAttractionId = $data["id"];
                                    $data = mysqli_query($_db, "SELECT ia.image as image
                                                                FROM imagesAttractions ia
                                                                WHERE ia.visitedAttractionId = $visitedAttractionId");
                                    if(mysqli_num_rows($data)==0){
                                        $output["images"] = "nessuna immagine";
                                    }else{
                                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                        $output["images"] = JSON_encode($data);
                                    }
                                    $output["result"] = "success";
                                }
                            }
                            
                        }
                        break;
                    case 'updateDonePlace':
                        if(!isset($_POST["id"]) || !isset($_POST["kind"])){
                            $output["message"]="NON sono stati inseriti tutti i campi necessari!";
                        }else{
                            if($_POST["kind"]=="city"){
                                if(isset($_POST["notes"])){
                                    mysqli_query($_db, "UPDATE visitedCities SET notes='".escape($_POST["notes"])."' WHERE id='".escape($_POST["id"])."'");
                                }
                                mysqli_query($_db, "UPDATE visitedCities SET preference='".escape($_POST["preferences"])."' WHERE id='".escape($_POST["id"])."'");
                            }else{
                                if(isset($_POST["notes"])){
                                    mysqli_query($_db, "UPDATE visitedAttractions SET notes='".escape($_POST["notes"])."' WHERE id='".escape($_POST["id"])."'");
                                }
                                mysqli_query($_db, "UPDATE visitedAttractions SET preference='".escape($_POST["preferences"])."' WHERE id='".escape($_POST["id"])."'");
                            }
                            $output["result"] = "success";
                        }
                        break;
                    case 'getEvents':
                        if(!isset($_POST["email"])){
                            $output["message"]="Non esiste un utente con tale email";
                        }else{
                            $data = mysqli_query($_db, "SELECT t.title,
                                                                t.departureDate,
                                                                t.returnDate,
                                                                t.status,
                                                                t.id
                                                            FROM trips t, 
                                                                    users u 
                                                            WHERE u.email='".escape($_POST["email"])."'
                                                                    and u.id = t.userId");
                            if(mysqli_num_rows($data)==0){
                                $output["message"] = "Non hai ancora organizzato alcun viaggio!";
                            }else{
                                $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                                $output["events"] = JSON_encode($data);
                                $output["result"] = "success";
                            }
                        }
                    break;
                    case 'addPlaceToTrip':
                        //controllo se la richiesta contiene tutti i campi necessari
                        if( !isset($_POST["title"]) || !isset($_POST["departureDate"]) || !isset($_POST["returnDate"]) || !isset($_POST["email"])
                            || !isset($_POST["kind"]) || !isset($_POST["name"])){
                            $output["message"]="Non sono stati inseriti tutti i campi";
                        }else{
                            //converte stringhe in date
                            $departureDate = date('Y-m-d',strtotime($_POST["departureDate"]));
                            $returnDate = date('Y-m-d',strtotime($_POST["returnDate"]));
                            $data = mysqli_query($_db, "SELECT t.id
                                                            FROM trips t, 
                                                                 users u 
                                                            WHERE u.email='".escape($_POST["email"])."'
                                                                  and u.id = t.userId
                                                                  and t.status='organized'
                                                                  and t.title='".escape($_POST["title"])."'
                                                                  and t.departureDate='$departureDate'
                                                                  and t.returnDate='$returnDate'");
                            if(mysqli_num_rows($data)==0){
                                $output["message"] = "Il viaggio che stai cercando non è mai stato organizzato!";
                            }else{
                                $idTrip=mysqli_fetch_assoc($data)["id"];
                                if($_POST["kind"]=="city"){
                                    $query = "SELECT id
                                              FROM cities 
                                              WHERE name='".escape($_POST["name"])."'";
                                    if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                        mysqli_query($_db, "INSERT INTO cities(name) VALUES ('".escape($_POST["name"])."')");
                                    }
                                    $data = mysqli_query($_db, $query);
                                    $idCity=mysqli_fetch_assoc($data)["id"];
                                    $data = mysqli_query($_db, "SELECT null
                                                            FROM visitedCities 
                                                            WHERE tripId = '$idTrip'
                                                                and cityId = '$idCity'");
                                    if(mysqli_num_rows($data)==0){
                                        mysqli_query($_db, "INSERT INTO visitedCities(tripId, cityId) VALUES ($idTrip, $idCity)");
                                    }
                                    $output["result"] = "success";
                                }else{
                                    $query = "SELECT id
                                              FROM attractions 
                                              WHERE name='".escape($_POST["name"])."'";
                                    if(mysqli_num_rows(mysqli_query($_db, $query))==0){
                                        mysqli_query($_db, "INSERT INTO attractions(name) VALUES ('".escape($_POST["name"])."')");
                                    }
                                    $data = mysqli_query($_db, $query);
                                    $idAttraction=mysqli_fetch_assoc($data)["id"];
                                    $data = mysqli_query($_db, "SELECT null
                                                            FROM visitedAttractions
                                                            WHERE tripId = $idTrip
                                                                and attractionId = $idAttraction");
                                    if(mysqli_num_rows($data)==0){
                                        mysqli_query($_db, "INSERT INTO visitedAttractions(tripId, attractionId) VALUES ($idTrip, $idAttraction)");
                                    }
                                    $output["result"] = "success";
                                }
                            }
                        }
                    break;
                    case 'updateOrganizedTrip':
                        //controllo se la richiesta contiene tutti i campi necessari
                        if( !isset($_POST["places"])){
                            $output["message"]="Non sono stati inseriti tutti i campi";
                        }else{
                            $places=ltrim($_POST["places"],"[");
                            $places=chop($places,"]");
                            $array=explode("/",$places);
                            for($i=0; $i<count($array); $i++){
                                $place = $array[$i];
                                $placeInfo = explode(",",$place);
                                if($placeInfo['2']=="city"){
                                	if($placeInfo['1']!="nessuna data"){
                                    	$visitedDate=date('Y-m-d',strtotime($placeInfo['1']));
                                        $id=$placeInfo['3'];
                                    	mysqli_query($_db, "UPDATE visitedCities SET visitedDate='$visitedDate' WHERE id=$id");
                                    }
                                }else{
                                	if($placeInfo['1']!="nessuna data"){
                                    	$visitedDate=date('Y-m-d',strtotime($placeInfo['1']));
                                        $id=$placeInfo['3'];
                                    	mysqli_query($_db, "UPDATE visitedAttractions SET visitedDate='$visitedDate' WHERE id=$id");
                                    }
                                }
                            }
                            $output["result"]="success";
                        }
                    break;    
        }
        output();
    }
    
?>
