package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ListAdapter.SearchListAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public class CalendarActivity extends AppCompatActivity {

    private SimpleDateFormat dateFormatMonth;
    private CompactCalendarView compactCalendar;
    private SearchListAdapter listAdapter;
    private List<String> values;
    private List<TripItem> trips;

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(CalendarActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_calendar);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.calendarConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        final ListView listView = this.findViewById(R.id.tripPlaceListView);
        this.listAdapter = new SearchListAdapter(this);
        listView.setAdapter(this.listAdapter);

        this.dateFormatMonth = new SimpleDateFormat("MMMM - yyyy", Locale.ITALY);
        this.trips = new ArrayList<>();
        this.values = new ArrayList<>();

        Utility.setUpToolbar(this, firstCharToUpperCase(this.dateFormatMonth.format(new Date())));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        this.compactCalendar = this.findViewById(R.id.compactcalendar_view);
        this.compactCalendar.setUseThreeLetterAbbreviation(true);
        this.compactCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        final Calendar calendar = new GregorianCalendar();
        final TimeZone timeZone = calendar.getTimeZone();
        this.compactCalendar.setLocale(timeZone, Locale.ITALY);

        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());

        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getEvents(this,email, compactCalendar,trips);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                final String title = values.get(position);
                TripItem myTrip = null;
                for(TripItem trip : trips){
                    final String name = trip.getName();
                    if(name.equals(title)){
                        myTrip = trip;
                        break;
                    }
                }

                if(myTrip != null){
                    final Intent i;
                    if(myTrip.getType().equals("done")){
                        i = new Intent(getApplicationContext(), DoneTripActivity.class);
                        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), DateAndCalendarUtilities.getCorrectDateFormatForOutput(myTrip.getDepartureDate()));
                        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), DateAndCalendarUtilities.getCorrectDateFormatForOutput(myTrip.getReturnDate()));
                    }else{
                        i = new Intent(getApplicationContext(), PlannedTripActivity.class);
                        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), myTrip.getDepartureDate());
                        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), myTrip.getReturnDate());
                    }
                    i.putExtra(Extra.TITLE.getTypeExtra(), myTrip.getName());
                    i.putExtra(Extra.ID.getTypeExtra(), myTrip.getTripId());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CalendarActivity");
                    startActivity(i);
                }
            }
        });

        this.compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                final List<Event> events = compactCalendar.getEvents(dateClicked);
                values.clear();
                for(int i=0; i<events.size(); i++){
                    values.add((String)events.get(i).getData());
                }
                listAdapter.setData(values);

                final TextView dateName = findViewById(R.id.dateTextView);
                final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALY);
                dateFormat.applyPattern(("dd-MM-yyyy"));
                dateName.setText(dateFormat.format(dateClicked).replace("-", "/"));
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Objects.requireNonNull(getSupportActionBar()).setTitle(firstCharToUpperCase(dateFormatMonth.format(firstDayOfNewMonth)));
            }
        });
    }

    private String firstCharToUpperCase(final String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
