package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.Objects;

public class PlannedTripsActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlannedTripsActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_plannedtrips);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.fragment_container);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(final Bundle savedInstanceState){
        final String nameActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        if (savedInstanceState == null) {
            if(Objects.requireNonNull(nameActivity).equals("MainActivity")
                    || Objects.requireNonNull(nameActivity).equals("PlannedTripActivity")
                    || Objects.requireNonNull(nameActivity).equals("AddInformationPlannedTrip")
                    || Objects.requireNonNull(nameActivity).equals("AddInformationPlannedTripActivity")
            ){
                Utility.insertFragment(this, new PlannedTripsFragment(), "plannedTripsFragment", R.id.fragment_container);
            }else{
                Utility.insertFragment(this, new AddPlannedTripsFragment(), "addPlannedTripsFragment", R.id.fragment_container);
            }
        }
    }
}
