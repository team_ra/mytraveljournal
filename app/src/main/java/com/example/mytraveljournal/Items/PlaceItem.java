package com.example.mytraveljournal.Items;

import androidx.annotation.NonNull;

public class PlaceItem {

    private final String name;
    private String date;
    private final String type;
    private final String id;

    public PlaceItem(final String name, final String date, final String type, final String id) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public String getType() {
        return this.type;
    }

    public String getId(){
        return this.id;
    }

    @NonNull
    @Override
    public String toString() {
        return "/"+name  +
                "," + date +
                "," + type +
                "," + id
                ;
    }

    public void setDate(final String date) {
        this.date = date;
    }
}
