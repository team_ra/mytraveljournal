package com.example.mytraveljournal.Items;

public class CardItem {

    private String imageResource;
    private String place;
    private String date;
    private String id;

    public CardItem(final String imageResource, final String place,
                    final String date, final String id) {
        this.imageResource = imageResource;
        this.place = place;
        this.date = date;
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getImageResource() {
        return this.imageResource;
    }

    public String getPlace() {
        return this.place;
    }

    public String getDate() {
        return this.date;
    }
}
