package com.example.mytraveljournal.Items;

import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;

public class TripItem {

    private final String tripId;
    private final String name;
    private final String type;
    private String departureDate;
    private String returnDate;

    public TripItem(final String tripId, final String name, final String type,
                    final String departureDate, final String returnDate) {
        this.name = name;
        this.type = type;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.tripId = tripId;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public String getDepartureDate() {
        return this.departureDate;
    }

    public String getReturnDate() {
        return this.returnDate;
    }

    public String getTripId() {
        return this.tripId;
    }

    public String getDate(){
        return DateAndCalendarUtilities.getCorrectDateFormatForOutput(this.departureDate) +
                " - " + DateAndCalendarUtilities.getCorrectDateFormatForOutput(this.returnDate);
    }

    public void setDepartureDate(final String departureDate) {
        this.departureDate = departureDate;
    }

    public void setReturnDate(final String returnDate) {
        this.returnDate = returnDate;
    }
}
