package com.example.mytraveljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Requests.UpdateRequests;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class UserInfoActivity extends AppCompatActivity {

    private String image;
    private ImageView userImage;
    private boolean changeImage;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(UserInfoActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_modificare_i_tuoi_dati)
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_userinfo);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.userInfoLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            this.userImage.setImageBitmap(bitmap);
            this.image = ImageUtilities.getStringImage(bitmap, true);
            this.changeImage = true;
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                this.userImage.setImageBitmap(bitmap);
                this.image = ImageUtilities.getStringImage(bitmap, false);
                this.changeImage = true;
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }

    private void initUI() {
        this.getUserData();

        this.changeImage = false;

        this.findViewById(R.id.modifyButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText usernameField = findViewById(R.id.modifyUsernameEditText);
                final EditText emailField = findViewById(R.id.modifyEmailEditText);
                final EditText oldPasswordField = findViewById(R.id.oldPasswordEditText);
                final EditText newPasswordField = findViewById(R.id.newPasswordEditText);
                final EditText repeatNewpassord = findViewById(R.id.repeatNewPasswordEditText);

                if (InternetUtilities.getIsNetworkConnected()) {
                    if(usernameField.getText().toString().isEmpty() && emailField.getText().toString().isEmpty() && repeatNewpassord.getText().toString().isEmpty()
                            && newPasswordField.getText().toString().isEmpty() && oldPasswordField.getText().toString().isEmpty() && !changeImage){
                        Utility.showToast(getString(R.string.nessun_dato_modificato), UserInfoActivity.this);
                    }else{
                        if ((newPasswordField.getText().toString().isEmpty() && !oldPasswordField.getText().toString().isEmpty() && !repeatNewpassord.getText().toString().isEmpty()) ||
                                (newPasswordField.getText().toString().isEmpty() && !oldPasswordField.getText().toString().isEmpty() && repeatNewpassord.getText().toString().isEmpty()) ||
                                (newPasswordField.getText().toString().isEmpty() && oldPasswordField.getText().toString().isEmpty() && !repeatNewpassord.getText().toString().isEmpty()) ||
                                (!newPasswordField.getText().toString().isEmpty() && oldPasswordField.getText().toString().isEmpty() && repeatNewpassord.getText().toString().isEmpty()) ||
                                (!newPasswordField.getText().toString().isEmpty() && !oldPasswordField.getText().toString().isEmpty() && repeatNewpassord.getText().toString().isEmpty()) ||
                                (!newPasswordField.getText().toString().isEmpty() && oldPasswordField.getText().toString().isEmpty() && !repeatNewpassord.getText().toString().isEmpty()) ||
                                !newPasswordField.getText().toString().equals(repeatNewpassord.getText().toString())){
                            Utility.showNeutralAlert(getString(R.string.attenzione), getString(R.string.per_modificare_la_password_inserire_la_vecchia), UserInfoActivity.this);
                        }else{
                            UpdateRequests.updateUserData(UserInfoActivity.this, usernameField, emailField, newPasswordField, image, changeImage);
                        }
                    }
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.findViewById(R.id.modifyAddPhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST);
            }
        });

        this.findViewById(R.id.modifyMakePhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE);
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.i_tuoi_dati));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getUserData() {
        final List<String> values = FileUtilities.getData(this.getCacheDir() + "/mtj.txt");
        final EditText username = this.findViewById(R.id.modifyUsernameEditText);
        final EditText email = this.findViewById(R.id.modifyEmailEditText);
        this.userImage = this.findViewById(R.id.modifyPhotoImageView);

        username.setHint(values.get(UserManagement.USERNAME.getId()));
        email.setHint(values.get(UserManagement.EMAIL.getId()));
        final StringBuilder stringBuilder = new StringBuilder();

        for (int i=2; i<values.size(); i++){
            stringBuilder.append(values.get(i)).append("\n");
        }
        this.image = stringBuilder.toString();
        this.userImage.setImageBitmap(ImageUtilities.decodeBitmap(this.image));
    }

}
