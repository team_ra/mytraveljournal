package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.RecyclerView.CardAdapter;
import com.example.mytraveljournal.Items.CardItem;
import com.example.mytraveljournal.Adapters.RecyclerView.OnItemListener;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DoneTripsActivity extends AppCompatActivity implements OnItemListener {

    private CardAdapter adapter;
    private List<CardItem> list;

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(DoneTripsActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    public void onItemClick(final int position) {
        final String departureDate = this.list.get(position).getDate().split("-")[0].replace("/", "-");
        final String returnDate = this.list.get(position).getDate().split("-")[1].replace("/", "-");
        final Intent i = new Intent(getApplicationContext(), DoneTripActivity.class);
        i.putExtra(Extra.TITLE.getTypeExtra(), this.list.get(position).getPlace());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), departureDate);
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), returnDate);
        i.putExtra(Extra.ID.getTypeExtra(), this.list.get(position).getId());
        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DoneTripsActivity");
        this.startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_donetrips);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.doneTripsActivity);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final RecyclerView recyclerView = this.findViewById(R.id.doneTripsRecyclerView);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        this.adapter = new CardAdapter(listener, this);
        recyclerView.setAdapter(this.adapter);

        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        this.list = new ArrayList<>();

        Utility.setUpToolbar(this, this.getString(R.string.viaggi_effettuati));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());
        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getDoneTrips(this, email, list, adapter, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

}
