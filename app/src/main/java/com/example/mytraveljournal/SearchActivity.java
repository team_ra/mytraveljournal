package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ListAdapter.SearchListAdapter;
import com.example.mytraveljournal.Requests.OsmUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchActivity extends AppCompatActivity {

    private List<String> placesList;
    private RadioGroup radioGroup;
    private String selectedPlace;
    private int selectedRadio;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_search);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.searchLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final ListView listView = this.findViewById(R.id.searchedPlaceListView);
        final EditText placeEditText = this.findViewById(R.id.searchEditText);
        this.radioGroup = this.findViewById(R.id.searchRadioGroup);
        this.placesList = new ArrayList<>();

        final SearchListAdapter listAdapter = new SearchListAdapter(this);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                selectedPlace = placesList.get(position);
                final Intent i;
                if(!selectedPlace.equals(getString(R.string.nessuna_attrazione_trovata)) &&
                        !selectedPlace.equals(getString(R.string.nessuna_citta_trovata))){
                    if(selectedRadio==R.id.cityRadioButton) {
                        i = new Intent(getApplicationContext(), CityActivity.class);
                        i.putExtra(Extra.CITY.getTypeExtra(), selectedPlace);
                    }else{
                        i = new Intent(getApplicationContext(), AttractionActivity.class);
                        i.putExtra(Extra.ATTRACTION.getTypeExtra(), selectedPlace);
                    }
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "SearchActivity");
                    startActivity(i);
                }
            }
        });

        this.findViewById(R.id.searchImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String place = placeEditText.getText().toString();
                placesList.clear();
                selectedRadio = radioGroup.getCheckedRadioButtonId();
                switch(selectedRadio) {
                    case -1:
                        Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), SearchActivity.this);
                        break;
                    case R.id.cityRadioButton:
                        if (InternetUtilities.getIsNetworkConnected()) {
                            OsmUtilities.createCityRequest(place, placesList, SearchActivity.this, listAdapter);
                        } else {
                            InternetUtilities.getSnackbar().show();
                        }
                        break;
                    case R.id.attractionRadioButton:
                        if (InternetUtilities.getIsNetworkConnected()) {
                            OsmUtilities.createAttractionRequest(place, placesList, SearchActivity.this, listAdapter);
                        } else {
                            InternetUtilities.getSnackbar().show();
                        }
                        break;
                }
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.ricerca_luoghi));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
