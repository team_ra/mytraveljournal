package com.example.mytraveljournal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.PlaceImageViewPagerAdapter;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.ViewPagerAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.Utility;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlannedPlaceImagesActivity extends AppCompatActivity {

    private List<String> images;
    private ViewPagerAdapter viewPagerAdapter;
    private String kind;
    private String name;
    private TripItem trip;
    private String cities;
    private String attractions;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlannedPlaceImagesActivity.class)){
            final Intent i = new Intent(getApplicationContext(), PlannedTripActivity.class);
            i.putExtra(Extra.ID.getTypeExtra(), this.trip.getTripId());
            i.putExtra(Extra.TITLE.getTypeExtra(), this.trip.getName());
            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), this.trip.getReturnDate());
            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), this.trip.getDepartureDate());
            i.putExtra(Extra.CITIES.getTypeExtra(), this.cities);
            i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), this.attractions);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlannedPlaceImagesActivity");
            this.startActivity(i);
        }else{
            this.finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_plannedplaceimages);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.plannedPlaceImagesLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            this.addImagesToTrip(bitmap, true);
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                ImageUtilities.saveImage(Objects.requireNonNull(bitmap),this);
                this.addImagesToTrip(bitmap, false);
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
        this.viewPagerAdapter.setData(this.images);
    }

    private void initUI(){
        final String tripId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        this.name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        this.kind = this.getIntent().getStringExtra(Extra.KIND.getTypeExtra());
        this.cities = this.getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
        this.attractions = this.getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
        this.trip = new TripItem(tripId, title, "planned", departureDate, returnDate);
        this.images = new ArrayList<>();

        final ViewPager2 viewPager = this.findViewById(R.id.plannedCityViewPager);
        viewPager.unregisterOnPageChangeCallback(ViewPagerAdapter.onPageChangeCallback);
        this.viewPagerAdapter = new PlaceImageViewPagerAdapter(this,this, this.kind, this.name, this.trip.getTripId());
        this.viewPagerAdapter.setDeletable(true);
        viewPager.setAdapter(this.viewPagerAdapter);
        viewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getImages(PlannedPlaceImagesActivity.this, this.kind, this.name, tripId, this.images, this.viewPagerAdapter, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.uploadPhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST);
            }
        });

        this.findViewById(R.id.takePictureImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE);
            }
        });

        Utility.setUpToolbar(this, this.name.split(";")[0]);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addImagesToTrip(final Bitmap image, final boolean isPicked){
        final Map<String,String> params = new HashMap<>();
        params.put("type", "addPlaceImage");
        params.put("kind", this.kind);
        params.put("name", this.name);
        params.put("tripId", this.trip.getTripId());
        params.put("image", ImageUtilities.getStringImage(image, isPicked));

        if (InternetUtilities.getIsNetworkConnected()) {
            AddRequests.addImagesToTrip(this, params, this.images, this.viewPagerAdapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }
}
