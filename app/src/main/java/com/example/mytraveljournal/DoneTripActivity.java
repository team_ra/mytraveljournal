package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.DoneTripExpandableListAdapter;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.DonePlaceImageViewPagerAdapter;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.ViewPagerAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DoneTripActivity extends AppCompatActivity {

    private String activityName;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(DoneTripActivity.class)){
            final Intent i;
            if(this.activityName.equals("CalendarActivity")){
                i = new Intent(getApplicationContext(), CalendarActivity.class);
            }else {
                i = new Intent(getApplicationContext(), DoneTripsActivity.class);
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_donetrip);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.donetripActivity);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final List<String> cities = new ArrayList<>();
        final List<String> attractions = new ArrayList<>();
        final List<String> images = new ArrayList<>();
        final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String tripId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        final TripItem tripItem = new TripItem(tripId, title, "done", departureDate, returnDate);

        final ExpandableListView listView = this.findViewById(R.id.showPlaceExpandableListView);
        final DoneTripExpandableListAdapter listAdapter = new DoneTripExpandableListAdapter(DoneTripActivity.this, DoneTripActivity.this, tripItem);
        listAdapter.setSelectable(true);
        listAdapter.setDeletable(false);
        listView.setAdapter(listAdapter);

        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        final ViewPager2 viewPager = this.findViewById(R.id.doneTripViewPager);
        viewPager.unregisterOnPageChangeCallback(ViewPagerAdapter.onPageChangeCallback);
        ViewPagerAdapter viewPagerAdapter = new DonePlaceImageViewPagerAdapter(this, this);
        viewPagerAdapter.setDeletable(false);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        final TextView dateTextView = this.findViewById(R.id.datePlaceTextView);
        final String date = Objects.requireNonNull(departureDate).replace("-", "/") +
                            " - " + Objects.requireNonNull(returnDate).replace("-", "/");
        dateTextView.setText(date);

        final Map<String, List<String>> values = new HashMap<>();
        values.put(this.getString(R.string.citta), cities);
        values.put(this.getString(R.string.attrazioni), attractions);
        listAdapter.setData(values);

        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getDoneTrip(this, tripItem.getTripId(), cities, attractions, images, listAdapter, viewPagerAdapter, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, title);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
