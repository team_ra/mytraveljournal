package com.example.mytraveljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.viewpager2.widget.ViewPager2;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.DonePlaceImageViewPagerAdapter;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.ViewPagerAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.UpdateRequests;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DonePlaceActivity extends AppCompatActivity {

    private List<AppCompatImageButton> imageButtons;
    private ViewPagerAdapter viewPagerAdapter;
    private List<String> images;
    private String placeId;
    private int count;
    private TripItem tripItem;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(DonePlaceActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_modificare_il_viaggio)
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = new Intent(getApplicationContext(), DoneTripActivity.class);
                            i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
                            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                            i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DonePlaceActivity");
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_doneplace);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.donePlaceActivity);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());
        final String tripId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String placeName = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String kind = this.getIntent().getStringExtra(Extra.KIND.getTypeExtra());
        final String travelName = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final String departureDateTravel = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDateTravel = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());

        this.tripItem = new TripItem(tripId,travelName,"done",departureDateTravel,returnDateTravel);

        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        final AppCompatImageButton oneStarButton = this.findViewById(R.id.starOneImageButton);
        final AppCompatImageButton twoStarButton = this.findViewById(R.id.starTwoImageButton);
        final AppCompatImageButton threeStarButton = this.findViewById(R.id.starThreeImageButton);
        final AppCompatImageButton fourStarButton = this.findViewById(R.id.starFourImageButton);
        final AppCompatImageButton fiveStarButton = this.findViewById(R.id.starFiveImageButton);
        this.imageButtons = new ArrayList<>();
        this.images = new ArrayList<>();

        this.imageButtons.add(0, oneStarButton);
        this.imageButtons.add(1, twoStarButton);
        this.imageButtons.add(2, threeStarButton);
        this.imageButtons.add(3, fourStarButton);
        this.imageButtons.add(4, fiveStarButton);

        for(int i=0; i< this.imageButtons.size(); i++){
            final int index = i;
            this.imageButtons.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setClicked(index);
                }
            });
        }

        this.findViewById(R.id.fab_add_doneplace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetUtilities.getIsNetworkConnected()) {
                    UpdateRequests.updateDoneTrip(DonePlaceActivity.this, (EditText)findViewById(R.id.commentDonePlaceEditText), count,
                            kind, placeId, tripItem);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        final ViewPager2 viewPager = this.findViewById(R.id.donePlaceViewPager);
        viewPager.unregisterOnPageChangeCallback(ViewPagerAdapter.onPageChangeCallback);
        this.viewPagerAdapter = new DonePlaceImageViewPagerAdapter(this, this);
        this.viewPagerAdapter.setDeletable(false);
        viewPager.setAdapter(this.viewPagerAdapter);
        viewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getPlaceInformation(email, departureDateTravel, returnDateTravel, travelName, kind, placeName,
                    (TextView)this.findViewById(R.id.datePlaceTextView), (EditText)this.findViewById(R.id.commentDonePlaceEditText),
                    loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, Objects.requireNonNull(placeName).split(";")[0]);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getPlaceInformation(final String email, final String departureDate, final String returnDate,
                                     final String title, final String kind, final String name, final TextView date,
                                     final EditText comments, final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final JSONObject place = (JSONObject)jsonObject.get("place");
                                String visitedDate, notes;
                                int preference;
                                placeId = place.getString("id");
                                if(!place.isNull("visitedDate")){
                                    visitedDate = place.getString("visitedDate");
                                    visitedDate = DateAndCalendarUtilities.getCorrectDateFormatForOutput(visitedDate);
                                }else{
                                    visitedDate = getString(R.string.nessuna_data);
                                }
                                date.setText(visitedDate);
                                if(!place.isNull("preference")){
                                    preference = place.getInt("preference");
                                    setClicked(preference-1);
                                }else{
                                    preference = -1;
                                    setClicked(preference);
                                }
                                if(!place.isNull("notes")){
                                    notes = place.getString("notes");
                                    comments.setText(notes);
                                }
                                if(!jsonObject.getString("images").equals(getString(R.string.nessuna_immagine))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("images"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject placeImages = (JSONObject) jsonArray.get(i);
                                        images.add(placeImages.getString("image"));
                                    }
                                }
                                loadingDialog.dismissDialog();
                                viewPagerAdapter.setData(images);
                            }else{
                                Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), DonePlaceActivity.this);
                            }
                        } catch (final JSONException e) {
                            Log.e(getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getDonePlace");
                params.put("email", email);
                params.put("title", title);
                params.put("departureDate", departureDate);
                params.put("returnDate", returnDate);
                params.put("kind", kind);
                params.put("name", name);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void setClicked(final int position){
        if(position == -1){
            for(int i=0; i< this.imageButtons.size(); i++){
                this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
            }
            this.count = 0;
        }else{
            for(int i=0; i< this.imageButtons.size(); i++){
                if(i<=position){
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_24);
                }else{
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
                }
            }
            this.count = position+1;
        }
    }
}
