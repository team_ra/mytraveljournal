package com.example.mytraveljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.ExpandableListAdapter;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.PlannedTripExpandableListAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Requests.UpdateRequests;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlannedTripActivity extends AppCompatActivity{

    private TripItem tripItem;
    private String activityName;
    private TextView departureDateTextView;
    private TextView returnDateTextView;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlannedTripActivity.class)){
            Intent i;
            if(activityName.equals("CalendarActivity")){
                i = new Intent(getApplicationContext(), CalendarActivity.class);
            }else{
                i = new Intent(getApplicationContext(), PlannedTripsActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlannedTripActivity");
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_plannedtrip);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.plannedTripLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String tripId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        this.tripItem = new TripItem(tripId, title, "planned", departureDate, returnDate);

        final ExpandableListView listView = this.findViewById(R.id.showExpandableListView);
        final ExpandableListAdapter listAdapter = new PlannedTripExpandableListAdapter(PlannedTripActivity.this, PlannedTripActivity.this, tripItem);
        listAdapter.setSelectable(true);
        listAdapter.setDeletable(true);
        listView.setAdapter(listAdapter);

        final List<String> cities = new ArrayList<>();
        final List<String> attractions = new ArrayList<>();
        this.departureDateTextView = this.findViewById(R.id.fixDepartureDateTextView);
        this.returnDateTextView = this.findViewById(R.id.fixReturnDateTextView);

        this.departureDateTextView.setText(Objects.requireNonNull(DateAndCalendarUtilities.getCorrectDateFormatForOutput(departureDate)).replace("/", "-"));
        this.returnDateTextView.setText(Objects.requireNonNull(DateAndCalendarUtilities.getCorrectDateFormatForOutput(returnDate)).replace("/", "-"));

        if (InternetUtilities.getIsNetworkConnected()) {
            if(this.activityName.equals("PlannedTripsActivity") || this.activityName.equals("CalendarActivity")){
                GetRequests.getTrip(this, tripId, cities, attractions, listAdapter);
            }else{
                final String citiesList = this.getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
                final String attractionsList= this.getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
                cities.clear();
                attractions.clear();
                try {
                    JSONArray jsonArray = new JSONArray(citiesList);
                    cities.addAll(Utility.JSONArrayToList(this, jsonArray));
                    jsonArray = new JSONArray(attractionsList);
                    attractions.addAll(Utility.JSONArrayToList(this, jsonArray));
                    final Map<String,String> params = new HashMap<>();
                    params.put("type", "updateTrip");
                    params.put("tripId", tripItem.getTripId());
                    if(!cities.isEmpty()){
                        params.put("cities", cities.toString());
                    }
                    if (!attractions.isEmpty()){
                        params.put("attractions",attractions.toString());
                    }
                    if (InternetUtilities.getIsNetworkConnected()) {
                        UpdateRequests.updateTripData(PlannedTripActivity.this, params,
                                getString(R.string.viaggio_aggiornato_con_successo));
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                } catch (JSONException e) {
                    Log.e(this.getString(R.string.error), e.toString());
                }
            }
        } else {
            InternetUtilities.getSnackbar().show();
        }

        final Map<String, List<String>> values = new HashMap<>();
        values.put(this.getString(R.string.citta), cities);
        values.put(this.getString(R.string.attrazioni), attractions);
        listAdapter.setData(values);

        this.findViewById(R.id.fixDepartureDateImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateAndCalendarUtilities.showDatePickerDialog(PlannedTripActivity.this, departureDateTextView, PlannedTripActivity.this, tripItem, true);
            }
        });

        this.findViewById(R.id.fixReturnDateImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateAndCalendarUtilities.showDatePickerDialog(PlannedTripActivity.this, returnDateTextView, PlannedTripActivity.this, tripItem, false);
            }
        });

        this.findViewById(R.id.plannedTripAddCityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(createIntent("city", cities, attractions));
            }
        });

        this.findViewById(R.id.plannedTripAddAttractionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(createIntent("attraction", cities, attractions));
            }
        });

        this.findViewById(R.id.organizeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(getApplicationContext(), OrganizeTripActivity.class);
                startActivity(addExtra(i, cities, attractions));
            }
        });

        /*this.findViewById(R.id.fab_add_plannedtrip).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateTrip");
                params.put("tripId", tripItem.getTripId());
                params.put("departureDate", tripItem.getDepartureDate());
                params.put("returnDate", tripItem.getReturnDate());
                if(!cities.isEmpty()){
                    params.put("cities", cities.toString());
                }
                if (!attractions.isEmpty()){
                    params.put("attractions",attractions.toString());
                }
                if (InternetUtilities.getIsNetworkConnected()) {
                    Intent i = new Intent(getApplicationContext(), PlannedTripsActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlannedTripActivity");
                    UpdateRequests.updateStandard(PlannedTripActivity.this, params,
                            getString(R.string.viaggio_aggiornato_con_successo), i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });*/

        Utility.setUpToolbar(this, this.tripItem.getName());
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private Intent createIntent(final String kind, final List<String> cities, final List<String> attractions){
        final Intent i = new Intent(getApplicationContext(), AddPlacePlannedTripActivity.class);
        i.putExtra(Extra.KIND.getTypeExtra(), kind);
        addExtra(i, cities, attractions);
        return i;
    }

    private Intent addExtra(final Intent i, final List<String> cities, final List<String> attractions){
        i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
        i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlannedTripActivity");
        JSONArray jsonArray = Utility.listToJSONArray(this, cities);
        i.putExtra(Extra.CITIES.getTypeExtra(), jsonArray.toString());
        jsonArray = Utility.listToJSONArray(this, attractions);
        i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), jsonArray.toString());
        return i;
    }
}
