package com.example.mytraveljournal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    private ImageView imageView;
    private String image;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_registration);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            this.imageView.setImageBitmap(bitmap);
            this.image = ImageUtilities.getStringImage(bitmap, true);
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                this.imageView.setImageBitmap(bitmap);
                this.image = ImageUtilities.getStringImage(bitmap, false);
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.registrationLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.imageView = this.findViewById(R.id.registrationImageView);

        this.findViewById(R.id.rgRegistrationButton).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                final EditText usernameField = findViewById(R.id.rgUsernameEditText);
                final EditText emailField = findViewById(R.id.rgEmailEditText);
                final EditText password1Field = findViewById(R.id.rgPassword1EditText);
                final EditText password2Field = findViewById(R.id.rgPassword2EditText);

                final String username = usernameField.getText().toString();
                final String email = emailField.getText().toString();
                final String password1 = password1Field.getText().toString();
                final String password2 = password2Field.getText().toString();

                if (InternetUtilities.getIsNetworkConnected()) {
                    if (!password1.equals(password2)) {
                        Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.due_password_non_coincidono), RegistrationActivity.this);
                    } else {
                        if (!username.isEmpty() && !email.isEmpty() && !password1.isEmpty() && image != null){
                            addRegistration(username, password1, email, image);
                        }else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), getString(R.string.dati_non_inseriti), RegistrationActivity.this);
                        }
                    }
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.findViewById(R.id.addPhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST);
            }
        });

        this.findViewById(R.id.makePhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE);
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.registrati));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addRegistration(final String username, final String password, final String email, final String image){
        final Map<String,String> params = new HashMap<>();
        params.put("type", "registration");
        params.put("username",username);
        params.put("email", email);
        params.put("password", password);
        params.put("image", image);

        AddRequests.addRegistration(this, params);
    }
}
