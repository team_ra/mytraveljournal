package com.example.mytraveljournal.Utilities;

public enum UserManagement {

    EMAIL(0),
    USERNAME(1),
    IMAGE(2);

    private int id;

    UserManagement(final int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }
}
