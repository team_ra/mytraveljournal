package com.example.mytraveljournal.Utilities;

public enum Extra {
    TITLE("title"),
    NAME("name"),
    DEPARTURE_DATE("departureDate"),
    RETURN_DATE("returnDate"),
    ACTIVITY_NAME("activityName"),
    CITY("city"),
    CITIES("cities"),
    ATTRACTIONS("attractions"),
    ATTRACTION("attraction"),
    KIND("kind"),
    ID("id");

    private String typeExtra;

    Extra(final String typeExtra){
        this.typeExtra = typeExtra;
    }

    public String getTypeExtra() {
        return this.typeExtra;
    }
}
