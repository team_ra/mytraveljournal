package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class DeleteRequests {

    public static void deleteElement(final Activity activity, final Map<String, String> params, final String message){
        String url ="https://mytravelj.altervista.org/index.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Utility.showToast(message, activity);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione),jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
