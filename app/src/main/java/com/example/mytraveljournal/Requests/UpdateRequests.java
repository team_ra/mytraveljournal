package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.mytraveljournal.DonePlaceActivity;
import com.example.mytraveljournal.DoneTripActivity;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.MainActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpdateRequests {

    //UserInfoActivity
    public static void updateUserData(final Activity activity, final EditText username, final EditText email,
                                      final EditText password, final String image, final boolean changeImage){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final String email = jsonObject.getString("email");
                                final String username = jsonObject.getString("username");
                                saveUserData(activity, email, username, image);
                                Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                                Utility.showToastIntent(activity.getString(R.string.informazioni_aggiornate_con_successo), activity, i);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione),jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateUserData");
                if(!username.getText().toString().isEmpty()){
                    params.put("username", username.getText().toString());
                }
                if(!email.getText().toString().isEmpty()){
                    params.put("email", email.getText().toString());
                }
                if(!password.getText().toString().isEmpty()){
                    params.put("password", password.getText().toString());
                }
                params.put("oldEmail", FileUtilities.getData(activity.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId()));
                if(changeImage){
                    params.put("image", image);
                }
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //OrganizeTripActivity
    public static void updateStandard(final Activity activity, final Map<String, String> params,
                                      final String message, final Intent i){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Utility.showToastIntent(message, activity, i);
                            } else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //DonePlaceActivity
    public static void updateDoneTrip(final Activity activity, final EditText notes, final int preferences,
                                      final String kind, final String placeId, final TripItem tripItem) {
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final Intent i = new Intent(activity.getApplicationContext(), DoneTripActivity.class);
                                i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
                                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DonePlaceActivity");
                                i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
                                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                                Utility.showToastIntent(activity.getString(R.string.viaggio_aggiornato_con_successo), activity, i);
                            } else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateDonePlace");
                params.put("id", placeId);
                params.put("kind", kind);
                if(!notes.getText().toString().isEmpty()){
                    params.put("notes", notes.getText().toString());
                }
                params.put("preferences", String.valueOf(preferences));
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //DateAndCalendarUtilities, PlannedTripActivity
    public static void updateTripData(final Activity activity, final Map<String, String> params,
                                      final String message){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Utility.showToast(message, activity);
                            } else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private static void saveUserData(final Activity activity, final String email, final String username, final String image) {
        String nameFile = activity.getCacheDir()+"/mtj.txt";
        List<String> values = new ArrayList<>();
        values.add(email);
        values.add(username);
        values.add(image);
        FileUtilities.saveData(values,nameFile);
    }
}
