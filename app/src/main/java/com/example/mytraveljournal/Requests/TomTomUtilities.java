package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.InternetUtilities;

class TomTomUtilities {

    static void getAttractionStaticMap(final Activity activity, final String latitude, final String longitude, final ImageView image, final int zoom){
        final String url = "https://api.tomtom.com/map/1/staticimage?key=GDD8GDKRcDe6bZS4QShoNgnC9jisbNNZ&zoom="+zoom+"&center="+longitude+","+latitude
                +"&format=jpg&layer=basic&style=main&width=1305&height=1400&view=Unified&language=en-GB";
        final ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(final Bitmap bitmap) {
                        image.setImageBitmap(bitmap);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER_CROP,null,
                new Response.ErrorListener() {
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(request);
    }

    static void getCityStaticMap(final Activity activity, final String latitude, final String longitude, final ImageView image){
        final String url = "https://api.tomtom.com/map/1/staticimage?key=GDD8GDKRcDe6bZS4QShoNgnC9jisbNNZ&zoom=13&center="+longitude+","+latitude
                +"&format=jpg&layer=basic&style=main&width=1305&height=1400&view=Unified&language=en-GB";
        final ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(final Bitmap bitmap) {
                        image.setImageBitmap(bitmap);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER_CROP,null,
                new Response.ErrorListener() {
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(request);
    }

}
