package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.DoneTripExpandableListAdapter;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.ExpandableListAdapter;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.ViewPagerAdapter;
import com.example.mytraveljournal.Adapters.ListAdapter.ListAdapter;
import com.example.mytraveljournal.Adapters.ListAdapter.PlaceDateListAdapter;
import com.example.mytraveljournal.Adapters.ListAdapter.PlannedTripsListAdapter;
import com.example.mytraveljournal.Adapters.RecyclerView.CardAdapter;
import com.example.mytraveljournal.Items.CardItem;
import com.example.mytraveljournal.Items.PlaceItem;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.MainActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.Utility;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GetRequests {

    //PlannedPlaceImagesActivity
    public static void getImages(final Activity activity, final String kind, final String name, final String tripId,
                                 final List<String> images, final ViewPagerAdapter viewPagerAdapter,
                                 final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("images").equals(activity.getString(R.string.nessuna_immagine))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("images"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject citiesInTrip = (JSONObject) jsonArray.get(i);
                                        images.add(citiesInTrip.getString("image"));
                                    }
                                }
                                loadingDialog.dismissDialog();
                                viewPagerAdapter.setData(images);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getPlaceImages");
                params.put("kind", kind);
                params.put("name", name);
                params.put("tripId", tripId);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //OrganizeTripActivity
    public static void getPlaceDates(final Activity activity, final String tripId, final List<PlaceItem> placeItemList,
                                     final PlaceDateListAdapter placeDateListAdapter){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("cities").equals(activity.getString(R.string.nessuna_citta))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("cities"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject citiesInTrip = (JSONObject) jsonArray.get(i);
                                        final String name = citiesInTrip.getString("name");
                                        final String date;
                                        if(!citiesInTrip.isNull("visitedDate")){
                                            date = Objects.requireNonNull(DateAndCalendarUtilities.
                                                    getCorrectDateFormatForOutput(citiesInTrip.getString("visitedDate"))).replace("/","-");
                                        }else{
                                            date = activity.getString(R.string.nessuna_data);
                                        }
                                        final String type = activity.getString(R.string.city);
                                        final PlaceItem city = new PlaceItem(name, date, type, citiesInTrip.getString("id"));
                                        placeItemList.add(city);
                                    }
                                }
                                if(!jsonObject.getString("attractions").equals(activity.getString(R.string.nessuna_attrazione))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("attractions"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject attractionsInTrip = (JSONObject) jsonArray.get(i);
                                        final String name = attractionsInTrip.getString("name");
                                        final String date;
                                        if(!attractionsInTrip.isNull("visitedDate")){
                                            date = Objects.requireNonNull(DateAndCalendarUtilities.
                                                    getCorrectDateFormatForOutput(attractionsInTrip.getString("visitedDate"))).replace("/","-");
                                        }else{
                                            date = activity.getString(R.string.nessuna_data);
                                        }
                                        final String type = activity.getString(R.string.attraction);
                                        final PlaceItem attraction = new PlaceItem(name, date, type, attractionsInTrip.getString("id"));
                                        placeItemList.add(attraction);
                                    }
                                }
                                placeDateListAdapter.setData(placeItemList);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getPlaceDates");
                params.put("tripId", tripId);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //MainActivity
    public static void getRandomTrip(final Activity activity, final String email,
                                     final List<CardItem> list, final CardAdapter adapter,
                                     final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                final JSONArray images = new JSONArray(jsonObject.getString("images"));
                                for (int i = 0; i <jsonArray.length(); i++) {
                                    final JSONObject randomTrips = (JSONObject) jsonArray.get(i);
                                    final String title = randomTrips.getString("title");
                                    final String id = randomTrips.getString("id");
                                    final String dates = DateAndCalendarUtilities.getCorrectDateFormatForOutput(randomTrips.getString("departureDate")) +
                                            " - " + DateAndCalendarUtilities.getCorrectDateFormatForOutput(randomTrips.getString("returnDate"));
                                    final String image;
                                    if (images.getString(i).equals(activity.getString(R.string.nessuna_immagine))) {
                                        image = images.getString(i);
                                    } else {
                                        image = images.getString(i);
                                    }
                                    final CardItem randomTrip = new CardItem(image, title, dates, id);
                                    list.add(randomTrip);
                                }
                                final RecyclerView recyclerView = activity.findViewById(R.id.homeRecyclerView);
                                final LinearLayout linearLayour = activity.findViewById(R.id.mainLinearLayout);
                                if(!list.isEmpty()){
                                    recyclerView.setVisibility(RecyclerView.VISIBLE);
                                    linearLayour.setVisibility(LinearLayout.GONE);
                                }
                                adapter.setData(list);
                            }
                            loadingDialog.dismissDialog();
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getRandomTrips");
                params.put("email", email);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //CalendarActivity
    public static void getEvents(final Activity activity, final String email, final CompactCalendarView compactCalendar, final List<TripItem> trips) {
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("events").equals(activity.getString(R.string.nessun_event))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("events"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject event = (JSONObject) jsonArray.get(i);
                                        final long departureDate = DateAndCalendarUtilities.getMillisTime(event.getString("departureDate"));
                                        final long returnDate = DateAndCalendarUtilities.getMillisTime(event.getString("returnDate"));
                                        long days = DateAndCalendarUtilities.getDaysBetweenTwoDates(departureDate, returnDate)-1;
                                        String date = event.getString("departureDate");
                                        for (int j=0; j<days ; j++){
                                            date =  DateAndCalendarUtilities.addDay(date);
                                            final Event ev = new Event(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorPrimaryDark), DateAndCalendarUtilities.getMillisTime(date) , event.getString("title"));
                                            compactCalendar.addEvent(ev);
                                        }
                                        Event ev = new Event(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorPrimaryDark), departureDate, event.getString("title"));
                                        compactCalendar.addEvent(ev);
                                        ev = new Event(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorPrimaryDark), returnDate, event.getString("title"));
                                        compactCalendar.addEvent(ev);
                                        final TripItem tripItem = new TripItem(event.getString("id"),
                                                event.getString("title"), event.getString("status"),
                                                event.getString("departureDate"), event.getString("returnDate"));
                                        trips.add(tripItem);
                                    }
                                }
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getEvents");
                params.put("email", email);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //LoginActivity
    public static void checkLogin(final Activity activity, final String email, final String password){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                saveUserData(activity, email, jsonObject.getString("username"), jsonObject.getString("image"));
                                final Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                                activity.startActivity(i);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "login");
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //DoneTripsActivity
    public static void getDoneTrips(final Activity activity, final String email,
                                    final List<CardItem> list, final CardAdapter adapter,
                                    final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                final JSONArray images = new JSONArray(jsonObject.getString("images"));
                                for (int i = 0; i <jsonArray.length(); i++){
                                    final JSONObject randomTrips = (JSONObject)jsonArray.get(i);
                                    final String title = randomTrips.getString("title");
                                    final String id = randomTrips.getString("id");
                                    final String dates = DateAndCalendarUtilities.getCorrectDateFormatForOutput(randomTrips.getString("departureDate")) +
                                            "-" + DateAndCalendarUtilities.getCorrectDateFormatForOutput(randomTrips.getString("returnDate"));
                                    final String image;
                                    if(images.getString(i).equals(activity.getString(R.string.nessuna_immagine))){
                                        image = images.getString(i);
                                    }else{
                                        image = images.getString(i);
                                    }
                                    final CardItem randomTrip = new CardItem(image, title, dates, id);
                                    list.add(randomTrip);
                                }
                                adapter.setData(list);
                            }
                            loadingDialog.dismissDialog();
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getDoneTrips");
                params.put("email", email);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //PlannedTripFragment, AddPlannedTripsFragment
    public static void getPlannedTrips(final Activity activity, final String email, final List<TripItem> values, final ListAdapter listAdapter){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i <jsonArray.length(); i++){
                                    final JSONObject plannedTrips = (JSONObject)jsonArray.get(i);
                                    final String tripId = plannedTrips.getString("id");
                                    final String title = plannedTrips.getString("title");
                                    final String departureDate =  plannedTrips.getString("departureDate");
                                    final String returnDate = plannedTrips.getString("returnDate");
                                    final String type = "planned";
                                    final TripItem planned = new TripItem(tripId, title, type, departureDate, returnDate);
                                    values.add(planned);
                                }
                                ((PlannedTripsListAdapter)listAdapter).setData(values);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getPlannedTrips");
                params.put("email", email);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //DoneTripActivity
    public static void getDoneTrip(final Activity activity, final String id, final List<String> cities,
                                   final List<String> attractions, final List<String> images,
                                   final DoneTripExpandableListAdapter listAdapter, final ViewPagerAdapter viewPagerAdapter,
                                   final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("cities").equals(activity.getString(R.string.nessuna_citta))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("cities"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject citiesInTrip = (JSONObject) jsonArray.get(i);
                                        cities.add(citiesInTrip.getString("name"));
                                    }
                                }
                                if(!jsonObject.getString("attractions").equals(activity.getString(R.string.nessuna_attrazione))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("attractions"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject attractionsInTrip = (JSONObject) jsonArray.get(i);
                                        attractions.add(attractionsInTrip.getString("name"));
                                    }
                                }
                                if(!jsonObject.getString("citiesImages").equals(activity.getString(R.string.nessuna_immagine))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("citiesImages"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject citiesImages = (JSONObject) jsonArray.get(i);
                                        images.add(citiesImages.getString("image"));
                                    }
                                }
                                if(!jsonObject.getString("attractionsImages").equals(activity.getString(R.string.nessuna_immagine))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("attractionsImages"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject attractionsImages = (JSONObject) jsonArray.get(i);
                                        images.add(attractionsImages.getString("image"));
                                    }
                                }
                                final Map<String, List<String>> values = new HashMap<>();
                                values.put(activity.getString(R.string.citta), cities);
                                values.put(activity.getString(R.string.attrazioni), attractions);
                                loadingDialog.dismissDialog();
                                listAdapter.setData(values);
                                viewPagerAdapter.setData(images);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getDoneTrip");
                params.put("id", id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //DesiredPlacesActivity
    public static void getDesiredPlaces(final Activity activity, final String email, final List<String> cities,
                                        final List<String> attractions, final ExpandableListAdapter listAdapter,
                                        final LoadingDialog loadingDialog){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("cities").equals(activity.getString(R.string.nessuna_citta))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("cities"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject citiesInTrip = (JSONObject) jsonArray.get(i);
                                        cities.add(citiesInTrip.getString("name"));
                                    }
                                }
                                if(!jsonObject.getString("attractions").equals(activity.getString(R.string.nessuna_attrazione))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("attractions"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject attractionsInTrip = (JSONObject) jsonArray.get(i);
                                        attractions.add(attractionsInTrip.getString("name"));
                                    }
                                }
                                final Map<String, List<String>> values = new HashMap<>();
                                values.put(activity.getString(R.string.citta), cities);
                                values.put(activity.getString(R.string.attrazioni), attractions);
                                listAdapter.setData(values);
                                loadingDialog.dismissDialog();
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getDesiredPlaces");
                params.put("email", email);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //PlannedTripActivity
    public static void getTrip(final Activity activity, final String tripId, final List<String> cities,
                         final List<String> attractions, final ExpandableListAdapter listAdapter){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                if(!jsonObject.getString("cities").equals(activity.getString(R.string.nessuna_citta))){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("cities"));
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        final JSONObject citiesInTrip = (JSONObject) jsonArray.get(i);
                                        cities.add(citiesInTrip.getString("name"));
                                    }
                                }
                                if(!jsonObject.getString("attractions").equals(activity.getString(R.string.nessuna_attrazione))) {
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("attractions"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject attractionsInTrip = (JSONObject) jsonArray.get(i);
                                        attractions.add(attractionsInTrip.getString("name"));
                                    }
                                }
                                final Map<String, List<String>> values = new HashMap<>();
                                values.put(activity.getString(R.string.citta), cities);
                                values.put(activity.getString(R.string.attrazioni), attractions);
                                listAdapter.setData(values);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getPlannedTrip");
                params.put("tripId", tripId);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private static void saveUserData(final Activity activity, final String email, final String username, final String image){
        final String nameFile = activity.getCacheDir()+"/mtj.txt";
        final List<String> values = new ArrayList<>();
        values.add(email);
        values.add(username);
        values.add(image);
        FileUtilities.saveData(values,nameFile);
    }
}
