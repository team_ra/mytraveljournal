package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.mytraveljournal.Adapters.ImageSliderAdapter.ViewPagerAdapter;
import com.example.mytraveljournal.MainActivity;
import com.example.mytraveljournal.PlannedTripsActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AddRequests {

    public static void addElement(final Activity activity, final Map<String, String> params, final String message){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Utility.showToast(message, activity);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //AddPlannedTripListAdapter
    public static void addPlaceToTrip(final Activity activity, final Map<String, String> params, final String message) {
        String url = "https://mytravelj.altervista.org/index.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                                Utility.showToastIntent(message, activity, i);
                            } else {
                                Utility.showNeutralAlert(jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.d(activity.getString(R.string.error), error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //RegistrationActivity
    public static void addRegistration(final Activity activity, final Map<String, String> params){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                saveUserData(activity, params.get("email"), params.get("username"), params.get("image"));
                                final Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                                activity.startActivity(i);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione),jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //PlannedPlaceImagesActivity
    public static void addImagesToTrip(final Activity activity, final Map<String, String> params,
                                       final List<String> images, final ViewPagerAdapter viewPagerAdapter){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                images.add(params.get("image"));
                                viewPagerAdapter.setData(images);
                                Utility.showToast(activity.getString(R.string.immagine_aggiunta_correttamente), activity);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    //AddInformationPlannedTripActivity
    public static void addPlannedTrip(final Activity activity, final Map<String, String> params){
        final String url ="https://mytravelj.altervista.org/index.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final Intent i = new Intent(activity.getApplicationContext(), PlannedTripsActivity.class);
                                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddInformationPlannedTripActivity");
                                activity.startActivity(i);
                            }else{
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private static void saveUserData(final Activity activity, final String email, final String username, final String image){
        final String nameFile = activity.getCacheDir()+"/mtj.txt";
        final List<String> values = new ArrayList<>();
        values.add(email);
        values.add(username);
        values.add(image);
        FileUtilities.saveData(values,nameFile);
    }
}
