package com.example.mytraveljournal.Requests;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.mytraveljournal.Adapters.ListAdapter.SearchListAdapter;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class OsmUtilities {

    public final static String OSM_REQUEST_TAG = "OSM_REQUEST";

    public static void createCityRequest(final String place, final List<String> list, final Activity activity,
                                         final SearchListAdapter listAdapter) {
        if(place.length()!=0){
            final String url = "https://nominatim.openstreetmap.org/search?city="+place+"&format=json";
            final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(final JSONArray response) {
                            try {
                                for(int i=0; i<response.length();i++){
                                    final JSONObject jsonobject = response.getJSONObject(i);
                                    list.add(Utility.formatOSMSring(jsonobject.get("display_name").toString()));
                                }
                                if(list.isEmpty()){
                                    list.add(0, activity.getResources().getString(R.string.nessuna_citta_trovata));
                                }
                                listAdapter.setData(list);
                            } catch (final JSONException e) {
                                Log.e(activity.getString(R.string.error), e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(final VolleyError error) {
                            Log.e(activity.getString(R.string.error), error.toString());
                        }
                    });
            jsonArrayRequest.setTag(OSM_REQUEST_TAG);
            InternetUtilities.getRequestQueue().add(jsonArrayRequest);
        }
    }

    public static void createAttractionRequest(final String place, final List<String> list, final Activity activity,
                                               final SearchListAdapter listAdapter) {
        if(place.length()!=0){
            final String url = "https://nominatim.openstreetmap.org/search?q="+place+"&format=json";
            final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(final JSONArray response) {
                            try {
                                for(int i=0; i<response.length();i++){
                                    final JSONObject jsonobject = response.getJSONObject(i);
                                    if(jsonobject.get("class").equals("historic") || jsonobject.get("class").equals("tourism") || jsonobject.get("class").equals("natural")
                                            || jsonobject.get("class").equals("geological")
                                            || (jsonobject.get("class").equals("landuse") && (jsonobject.get("type").equals("forest") || jsonobject.get("type").equals("cemetery")
                                            || jsonobject.get("type").equals("quarry") || jsonobject.get("type").equals("religious") || jsonobject.get("type").equals("salt_pond")
                                            || jsonobject.get("type").equals("reservoir") || jsonobject.get("type").equals("port")))
                                            || (jsonobject.get("class").equals("waterway") && (jsonobject.get("type").equals("waterfall") || jsonobject.get("type").equals("canal")
                                            || jsonobject.get("type").equals("river") || jsonobject.get("type").equals("stream") || jsonobject.get("type").equals("dam")
                                            || jsonobject.get("type").equals("weir")))
                                            || (jsonobject.get("class").equals("boundary") && (jsonobject.get("type").equals("national_park") || jsonobject.get("type").equals("protected_area")
                                            || jsonobject.get("type").equals("aboriginal_lands")))
                                            || (jsonobject.get("class").equals("building") && (jsonobject.get("type").equals("cathedral") || jsonobject.get("type").equals("chapel")
                                            || jsonobject.get("type").equals("church") || jsonobject.get("type").equals("mosque") || jsonobject.get("type").equals("religious")
                                            || jsonobject.get("type").equals("shrine") || jsonobject.get("type").equals("synagogue") || jsonobject.get("type").equals("temple")
                                            || jsonobject.get("type").equals("riding_hall") || jsonobject.get("type").equals("sports_hall") || jsonobject.get("type").equals("stadium")
                                            || jsonobject.get("type").equals("ruins") || jsonobject.get("type").equals("bridge")))
                                            || (jsonobject.get("class").equals("amenity") && (jsonobject.get("type").equals("place_of_worship") || jsonobject.get("type").equals("casino")
                                            || jsonobject.get("type").equals("theatre") || jsonobject.get("type").equals("planetarium") || jsonobject.get("type").equals("fountain")
                                            || jsonobject.get("type").equals("cinema") ||jsonobject.get("type").equals("casino") || jsonobject.get("type").equals("arts_centre")
                                            || jsonobject.get("type").equals("clock") || jsonobject.get("type").equals("monastery") || jsonobject.get("type").equals("public_bath")))
                                            || (jsonobject.get("class").equals("barrier") && (jsonobject.get("type").equals("ditch") || jsonobject.get("type").equals("city_wall")
                                            || jsonobject.get("type").equals("sally_port")))
                                            || (jsonobject.get("class").equals("leisure") && (!jsonobject.get("type").equals("slipway") && !jsonobject.get("type").equals("marina")
                                            && !jsonobject.get("type").equals("fitness_centre") && !jsonobject.get("type").equals("common")))){
                                        list.add(Utility.formatOSMSring(jsonobject.get("display_name").toString()));
                                    }
                                }
                                if(list.isEmpty()){
                                    list.add(0,activity.getResources().getString(R.string.nessuna_attrazione_trovata));
                                }
                                listAdapter.setData(list);
                            } catch (final JSONException e) {
                                Log.e(activity.getString(R.string.error), e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(final VolleyError error) {
                            Log.e(activity.getString(R.string.error), error.toString());
                        }
                    });
            jsonArrayRequest.setTag(OSM_REQUEST_TAG);
            InternetUtilities.getRequestQueue().add(jsonArrayRequest);
        }
    }

    public static void createGpsRequest(final Activity activity,final String latitude, final String longitude, final EditText text){
        final String url ="https://nominatim.openstreetmap.org/reverse?lat="+latitude+"&lon="+longitude+"&format=json&limit=1";
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            text.setText(response.get("display_name").toString().replace(",",";"));
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                });
        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    public static void createCityInfoRequest(final Activity activity,final String longName, final String city, final String country,final TextView lat,
                                             final TextView lon, final TextView population, final TextView capital, final TextView website,
                                             final ImageView imageView){
        final String url ="https://nominatim.openstreetmap.org/search?city="+city+"&country="+country+"&extratags=1&format=json";
        final JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        try {
                            for (int i = 0; i <response.length(); i++){
                                final JSONObject citiesList = (JSONObject)response.get(i);
                                if(citiesList.get("display_name").equals(longName)){
                                    String value;
                                    if(citiesList.has("lat")){
                                        value = lat.getText()+" "+citiesList.get("lat").toString();
                                    }else{
                                        value = lat.getText()+" "+activity.getString(R.string.non_trovata);
                                    }
                                    lat.setText(value);
                                    if(citiesList.has("lon")){
                                        value = lon.getText()+" "+citiesList.get("lon").toString();
                                    }else{
                                        value = lon.getText()+" "+activity.getString(R.string.non_trovata);
                                    }
                                    lon.setText(value);
                                    if(citiesList.has("extratags")){
                                        final JSONObject extraTag = (JSONObject) citiesList.get("extratags");
                                        if(extraTag.has("population")){
                                            value = population.getText()+" "+extraTag.get("population").toString();

                                        }else{
                                            value = population.getText()+" "+activity.getString(R.string.non_trovata);
                                        }
                                        population.setText(value);
                                        if(extraTag.has("capital") && extraTag.get("capital").equals("yes")){
                                            value = capital.getText()+" "+"sì";
                                        } else{
                                            value = capital.getText()+" "+"no";
                                        }
                                        capital.setText(value);
                                        if(extraTag.has("website")){
                                            value = website.getText()+" "+extraTag.get("website").toString();
                                        }else{
                                            value = website.getText()+" "+activity.getString(R.string.non_trovato);
                                        }
                                        website.setText(value);
                                        TomTomUtilities.getCityStaticMap(activity, citiesList.get("lat").toString(),
                                                citiesList.get("lon").toString(), imageView);
                                    }
                                    break;
                                }
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                });
        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    public static void createAttractionInfoRequest(final Activity activity, final String longName, final String attraction, final TextView lat,
                                                   final TextView lon, final TextView website, final TextView hours, final TextView phone,
                                                   final ImageView imageView){
        final String url ="https://nominatim.openstreetmap.org/search?q="+attraction+"&extratags=1&format=json";
        final JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        try {
                            for (int i = 0; i <response.length(); i++){
                                final JSONObject attractionsList = (JSONObject)response.get(i);
                                StringBuilder value;
                                int zoom;
                                if(attractionsList.get("display_name").equals(longName)){
                                    if(attractionsList.has("lat")){
                                        value = new StringBuilder(attractionsList.get("lat").toString());
                                    }else{
                                        value = new StringBuilder(activity.getString(R.string.non_trovata));
                                    }
                                    lat.setText(value.toString());
                                    if(attractionsList.has("lon")){
                                        value = new StringBuilder(attractionsList.get("lon").toString());
                                    }else{
                                        value = new StringBuilder(activity.getString(R.string.non_trovata));
                                    }
                                    lon.setText(value.toString());
                                    if(attractionsList.has("extratags")){
                                        final JSONObject extraTag = (JSONObject) attractionsList.get("extratags");
                                        if(extraTag.has("phone")){
                                            value = new StringBuilder(extraTag.get("phone").toString());
                                        }else{
                                            value = new StringBuilder(activity.getString(R.string.non_trovato));
                                        }
                                        phone.setText(value.toString());
                                        if(extraTag.has("opening_hours")){
                                            List<String> list = formatDate(extraTag.get("opening_hours").toString());
                                            value = new StringBuilder();
                                            for(int j=0;j<list.size();j++){
                                                if(j==list.size()-1){
                                                    value.append(list.get(j));
                                                }else{
                                                    value.append(list.get(j)).append("\n").append("\n");
                                                }
                                            }
                                        } else{
                                            value = new StringBuilder(activity.getString(R.string.non_trovati));
                                        }
                                        hours.setText(value.toString());
                                        if(extraTag.has("website")){
                                            value = new StringBuilder(extraTag.get("website").toString());
                                        }else{
                                            value = new StringBuilder(activity.getString(R.string.non_trovato));
                                        }
                                        website.setText(value.toString());
                                        if(attractionsList.get("class").equals("geological") || attractionsList.get("class").equals("waterway")
                                                || attractionsList.get("class").equals("natural") || attractionsList.get("class").equals("boundary")){
                                            zoom = 12;
                                        } else{
                                            zoom = 16;
                                        }
                                        TomTomUtilities.getAttractionStaticMap(activity,attractionsList.get("lat").toString(),
                                                attractionsList.get("lon").toString(), imageView, zoom);
                                    }
                                    break;
                                }
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                });
        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private static List<String> formatDate(final String openingHours){
        String hours = openingHours.replace("Mo","Lun");
        hours = hours.replace("Su","Dom");
        hours = hours.replace("Tu","Mar");
        hours = hours.replace("Th","Gio");
        hours = hours.replace("Sa","Sab");
        hours = hours.replace("Fr","Ven");
        hours = hours.replace("We","Mer");
        hours = hours.replace("Jan","Gen");
        hours = hours.replace("May","Mag");
        hours = hours.replace("Jun","Giu");
        hours = hours.replace("Jul","Lug");
        hours = hours.replace("Aug","Ago");
        hours = hours.replace("Sep","Set");
        hours = hours.replace("Oct","Ott");
        hours = hours.replace("Dec","Dic");
        hours = hours.replace("off","chiuso");
        hours = hours.replace("[","");
        hours = hours.replace("]","");
        return Arrays.asList(hours.split(";"));
    }
}
