package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.CitiesAndAttractionsExpandableListAdapter;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.ExpandableListAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CitiesAndAttractionsListActivity extends AppCompatActivity {

    private TripItem tripItem;
    private List<String> citiesList;
    private List<String> attractionsList;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(CitiesAndAttractionsListActivity.class)){
            final Intent i = new Intent(getApplicationContext(), AddInformationPlannedTripActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CitiesAndAttractionsListActivity");
            i.putExtra(Extra.TITLE.getTypeExtra(), this.tripItem.getName());
            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), this.tripItem.getDepartureDate());
            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), this.tripItem.getReturnDate());
            JSONArray jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, this.citiesList);
            i.putExtra(Extra.CITIES.getTypeExtra(), jsonArray.toString());
            jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, this.attractionsList);
            i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), jsonArray.toString());
            this.startActivity(i);
        }else{
            this.finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_citiesandattractionslist);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.citiesAndAttractionsListLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        this.tripItem = new TripItem("nessuno", title, "planned", departureDate, returnDate);
        final String cities = this.getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
        final String attractions = this.getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());

        final ExpandableListView listView = this.findViewById(R.id.addExpandableListView);
        final ExpandableListAdapter listAdapter = new CitiesAndAttractionsExpandableListAdapter(this);
        listAdapter.setSelectable(false);
        listAdapter.setDeletable(true);
        listView.setAdapter(listAdapter);

        this.citiesList = new ArrayList<>();
        this.attractionsList = new ArrayList<>();
        try {
            final JSONArray citiesArray = new JSONArray(cities);
            final JSONArray attractionsArray = new JSONArray(attractions);
            this.citiesList.clear();
            this.attractionsList.clear();
            this.citiesList.addAll(Utility.JSONArrayToList(this, citiesArray));
            this.attractionsList.addAll(Utility.JSONArrayToList(this, attractionsArray));
        } catch (JSONException e) {
            Log.e(this.getString(R.string.error), e.toString());
        }

        final HashMap<String, List<String>> values = new HashMap<>();
        values.put(this.getString(R.string.citta), this.citiesList);
        values.put(this.getString(R.string.attrazioni), this.attractionsList);
        listAdapter.setData(values);

        this.findViewById(R.id.addCityToTripButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddPlacePlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CitiesAndAttractionsListActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "city");
                i.putExtra(Extra.TITLE.getTypeExtra(), title);
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), departureDate);
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), returnDate);
                JSONArray jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, citiesList);
                i.putExtra(Extra.CITIES.getTypeExtra(), jsonArray.toString());
                jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, attractionsList);
                i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), jsonArray.toString());
                startActivity(i);
            }
        });

        this.findViewById(R.id.addAttractionToTripButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddPlacePlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CitiesAndAttractionsListActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "attraction");
                i.putExtra(Extra.TITLE.getTypeExtra(), title);
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), departureDate);
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), returnDate);
                JSONArray jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, citiesList);
                i.putExtra(Extra.CITIES.getTypeExtra(), jsonArray.toString());
                jsonArray = Utility.listToJSONArray(CitiesAndAttractionsListActivity.this, attractionsList);
                i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), jsonArray.toString());
                startActivity(i);
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.programma_un_viaggio));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
