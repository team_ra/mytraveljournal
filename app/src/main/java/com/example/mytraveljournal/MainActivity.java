package com.example.mytraveljournal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.RecyclerView.CardAdapter;
import com.example.mytraveljournal.Items.CardItem;
import com.example.mytraveljournal.Adapters.RecyclerView.OnItemListener;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnItemListener, NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private List<CardItem> list;

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(MainActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final String title = item.getTitle().toString();
        final Intent i;
        switch(title) {
            case "Viaggi Effettuati":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), DoneTripsActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Viaggi Programmati":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), PlannedTripsActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Luoghi dei Desideri":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), DesiredPlacesActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Calendario dei Viaggi":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), CalendarActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Mappe":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), MapActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Ricerca Luoghi":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), SearchActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "I tuoi Dati":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), UserInfoActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Logout":
                FileUtilities.removeFile(this.getCacheDir() + "/mtj.txt");
                i = new Intent(this.getApplicationContext(), LoginActivity.class);
                this.startActivity(i);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.homeDrawerLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    public void onItemClick(final int position) {
        final String departureDate = this.list.get(position).getDate().split("-")[0].replace("/", "-");
        final String returnDate = this.list.get(position).getDate().split("-")[1].replace("/", "-");
        final Intent i = new Intent(this.getApplicationContext(), DoneTripActivity.class);
        i.putExtra(Extra.TITLE.getTypeExtra(), this.list.get(position).getPlace());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), departureDate);
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), returnDate);
        i.putExtra(Extra.ID.getTypeExtra(), this.list.get(position).getId());
        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
        this.startActivity(i);
    }

    private void initUI(){
        final RecyclerView recyclerView = this.findViewById(R.id.homeRecyclerView);
        final LoadingDialog loadingDialog = new LoadingDialog(this);

        loadingDialog.startLoadingDialog();

        this.list = new ArrayList<>();

        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        final CardAdapter adapter = new CardAdapter(listener, this);
        recyclerView.setAdapter(adapter);

        if (!InternetUtilities.getIsNetworkConnected()) {
            InternetUtilities.getSnackbar().show();
        }else{
            GetRequests.getRandomTrip(this,email,this.list, adapter, loadingDialog);
        }

        this.findViewById(R.id.fab_add_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddInformationPlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                startActivity(i);
            }
        });

        final DrawerLayout drawerLayout = this.findViewById(R.id.homeDrawerLayout);
        this.navigationView=this.findViewById(R.id.navView);
        final Toolbar toolbar = this.findViewById(R.id.app_bar);
        this.setSupportActionBar(toolbar);

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.open_nav_drawer,
                R.string.close_nav_drawer
        );

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        this.navigationView.setNavigationItemSelectedListener(this);
        this.getUserData();
    }

    private void getUserData() {
        final List<String> values = FileUtilities.getData(this.getCacheDir() + "/mtj.txt");
        final View headerView = this.navigationView.getHeaderView(0);

        final TextView navUsername = headerView.findViewById(R.id.mnUserTextView);
        navUsername.setText(values.get(UserManagement.USERNAME.getId()));

        final ImageView userImage = headerView.findViewById(R.id.userImageView);
        userImage.setImageBitmap(ImageUtilities.decodeBitmap(values));
    }
}
