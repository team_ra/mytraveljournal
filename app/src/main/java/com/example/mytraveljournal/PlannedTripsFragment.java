package com.example.mytraveljournal;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import com.example.mytraveljournal.Adapters.ListAdapter.PlannedTripsListAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlannedTripsFragment extends Fragment {

    private PlannedTripsListAdapter listAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_plannedtrips, container, false);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                listAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();
        if (activity != null) {
            final ListView listView = activity.findViewById(R.id.plannedTripsListView);
            this.listAdapter = new PlannedTripsListAdapter(activity.getApplicationContext(),activity);
            listView.setAdapter(this.listAdapter);
            final List<TripItem> values = new ArrayList<>();

            final String email = FileUtilities.getData( activity.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());

            if (InternetUtilities.getIsNetworkConnected()) {
                GetRequests.getPlannedTrips(activity, email, values, this.listAdapter);
            } else {
                InternetUtilities.getSnackbar().show();
            }

            Utility.setUpToolbar(((PlannedTripsActivity)activity), getString(R.string.viaggi_programmati));
            Objects.requireNonNull( ((PlannedTripsActivity)activity).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        }else{
            Log.e(getString(R.string.error), getString(R.string.activity_is_null));
        }
    }
}
