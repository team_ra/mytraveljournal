package com.example.mytraveljournal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ListAdapter.SearchListAdapter;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.OsmUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.GpsUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;

import com.example.mytraveljournal.Utilities.PermissionsUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddPlacePlannedTripActivity extends AppCompatActivity {

    private List<String> placesList;
    private String selectedPlace;
    private String prevActivityName;
    private EditText gpsEditText;
    private TripItem tripItem;
    private String type;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                final String lat = intent.getStringExtra("location_latitude");
                final String lon = intent.getStringExtra("location_longitude");
                if (InternetUtilities.getIsNetworkConnected()) {
                    OsmUtilities.createGpsRequest(AddPlacePlannedTripActivity.this, lat, lon, gpsEditText);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                GpsUtilities.removeLocationUpdates(AddPlacePlannedTripActivity.this, broadcastReceiver);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (this.getClass().equals(AddPlacePlannedTripActivity.class)) {
                this.onBackPressed();
                this.finish();
            } else {
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtilities.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtilities.createSnackBar(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_addplaceplannedtrip);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.addPlaceLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        final String citiesList;
        final String attractionsList;
        final ListView listView = this.findViewById(R.id.searchedplaceListView);
        this.placesList = new ArrayList<>();

        final SearchListAdapter listAdapter = new SearchListAdapter(this);
        listView.setAdapter(listAdapter);

        final EditText placeEditText = this.findViewById(R.id.searchPlaceEditText);
        this.prevActivityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        this.type = this.getIntent().getStringExtra(Extra.KIND.getTypeExtra());
        this.gpsEditText = this.findViewById(R.id.gpsPlaceEditText);

        if(this.prevActivityName.equals("PlannedTripActivity")){
            final String title = getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
            final String departureDate = getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            final String tripId = getIntent().getStringExtra(Extra.ID.getTypeExtra());
            citiesList = getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
            attractionsList= getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
            this.tripItem = new TripItem(tripId, title, "planned", departureDate, returnDate);
        }else if (this.prevActivityName.equals("CitiesAndAttractionsListActivity")){
            final String title = getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
            final String departureDate = getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            citiesList = getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
            attractionsList = getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
            this.tripItem = new TripItem(this.getString(R.string.non_trovato), title, "planned", departureDate, returnDate);
        }else{
            citiesList = null;
            attractionsList = null;
        }

        if (!PermissionsUtilities.checkPermissions(this)) {
            PermissionsUtilities.requestPermissions(this);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            GpsUtilities.setLocation(this);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                selectedPlace = placesList.get(position);
                placeEditText.setText(selectedPlace);
            }
        });

        this.findViewById(R.id.fab_add_addplace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i;
                if(!gpsEditText.getText().toString().isEmpty()){
                    selectedPlace = gpsEditText.getText().toString();
                }
                if(prevActivityName.equals("CitiesAndAttractionsListActivity")){
                    i = new Intent(getApplicationContext(), CitiesAndAttractionsListActivity.class);
                    i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddPlacePlannedTripActivity");
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                } else if(prevActivityName.equals("PlannedTripActivity")){
                    i = new Intent(getApplicationContext(), PlannedTripActivity.class);
                    i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                    i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddPlacePlannedTripActivity");
                } else{
                    i = new Intent(getApplicationContext(), DesiredPlacesActivity.class);
                }
                if(selectedPlace != null && !selectedPlace.equals("")){
                    try {
                        if(type.equals("attraction") && !selectedPlace.equals(getString(R.string.nessuna_attrazione_trovata))){
                            if(prevActivityName.equals("CitiesAndAttractionsListActivity") || prevActivityName.equals("PlannedTripActivity")){
                                final JSONArray attractionsArray = new JSONArray(attractionsList);
                                final List<String> attractions = Utility.JSONArrayToList(AddPlacePlannedTripActivity.this, attractionsArray);
                                attractions.add(selectedPlace);
                                i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), Utility.listToJSONArray(AddPlacePlannedTripActivity.this, attractions).toString());
                                i.putExtra(Extra.CITIES.getTypeExtra(), citiesList);
                            } else{
                                i.putExtra(Extra.KIND.getTypeExtra(), getString(R.string.attraction));
                                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddPlacePlannedTripActivity");
                                i.putExtra(Extra.NAME.getTypeExtra(), selectedPlace);
                            }
                        }
                        else if(type.equals("city") && !selectedPlace.equals(getString(R.string.nessuna_citta_trovata))){
                            if(prevActivityName.equals("CitiesAndAttractionsListActivity") || prevActivityName.equals("PlannedTripActivity")){
                                final JSONArray citiesArray = new JSONArray(citiesList);
                                final List<String> cities = Utility.JSONArrayToList(AddPlacePlannedTripActivity.this, citiesArray);
                                cities.add(selectedPlace);
                                i.putExtra(Extra.CITIES.getTypeExtra(), Utility.listToJSONArray(AddPlacePlannedTripActivity.this, cities).toString());
                                i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), attractionsList);
                            } else{
                                i.putExtra(Extra.KIND.getTypeExtra(), getString(R.string.city));
                                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddPlacePlannedTripActivity");
                                i.putExtra(Extra.NAME.getTypeExtra(), selectedPlace);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                startActivity(i);
            }
        });

        this.findViewById(R.id.gpsImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeEditText.setText("");
                placesList.clear();
                listAdapter.setData(placesList);
                if (InternetUtilities.getIsNetworkConnected()) {
                    GpsUtilities.requestLocation(AddPlacePlannedTripActivity.this, broadcastReceiver);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.findViewById(R.id.searchPlaceImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String place = placeEditText.getText().toString();
                gpsEditText.setText("");
                placesList.clear();
                listAdapter.setData(placesList);
                if(type.equals("city")){
                    if (InternetUtilities.getIsNetworkConnected()) {
                        OsmUtilities.createCityRequest(place, placesList, AddPlacePlannedTripActivity.this, listAdapter);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                }else{
                    if (InternetUtilities.getIsNetworkConnected()) {
                        OsmUtilities.createAttractionRequest(place, placesList, AddPlacePlannedTripActivity.this, listAdapter);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                }
            }
        });

        if(this.type.equals(this.getString(R.string.city))){
            Utility.setUpToolbar(this, this.getString(R.string.aggiungi_una_citta));
            placeEditText.setHint(this.getString(R.string.ricerca_citta));
        }else{
            Utility.setUpToolbar(this, this.getString(R.string.aggiungi_un_attrazione));
            placeEditText.setHint(this.getString(R.string.ricerca_attrazione));
        }

        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
