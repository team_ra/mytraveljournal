package com.example.mytraveljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class AddInformationPlannedTripActivity extends AppCompatActivity{

    private String cities;
    private String attractions;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(AddInformationPlannedTripActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_aggiungere_il_viaggio)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = new Intent(getApplicationContext(), PlannedTripsActivity.class);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddInformationPlannedTrip");
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_addinformationtrip);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.addInformationLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final EditText titleEditText = this.findViewById(R.id.addTitleEditText);
        final EditText departureDateEditText = this.findViewById(R.id.addDepartureDateEditText);
        final EditText returnDateEditText = this.findViewById(R.id.addReturnDateEditText);
        final String prevActivityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        this.findViewById(R.id.returnDateImageButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                DateAndCalendarUtilities.showDatePickerDialog(AddInformationPlannedTripActivity.this, returnDateEditText);
            }
        });

        this.findViewById(R.id.departureDateImageButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                DateAndCalendarUtilities.showDatePickerDialog(AddInformationPlannedTripActivity.this, departureDateEditText);
            }
        });


        if(Objects.requireNonNull(prevActivityName).equals("CitiesAndAttractionsListActivity")){
            final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
            final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            if(getIntent().getStringExtra(Extra.CITIES.getTypeExtra())!=null){
                this.cities = getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
            }else{
                this.cities = "[]";
            }
            if(getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra())!=null){
                this.attractions = getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
            }else{
                this.attractions = "[]";
            }
            titleEditText.setText(title);
            departureDateEditText.setText(departureDate);
            returnDateEditText.setText(returnDate);
        }else{
            this.cities = "[]";
            this.attractions = "[]";
        }

        this.findViewById(R.id.listCitiesAndAttractionsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title = titleEditText.getText().toString();
                final String departureDate = departureDateEditText.getText().toString();
                final String returnDate = returnDateEditText.getText().toString();

                if (!title.equals("") && !departureDate.equals("") && !returnDate.equals("")){
                    if(DateAndCalendarUtilities.checkDataInput(departureDate)
                            && DateAndCalendarUtilities.checkDataInput(returnDate)
                            && checkDates(departureDate, returnDate)){
                        final TripItem tripItem = new TripItem("nessuno", title, "planned", getCorrectFormatData(departureDate), getCorrectFormatData(returnDate));
                        final Intent i = new Intent(getApplicationContext(), CitiesAndAttractionsListActivity.class);
                        i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
                        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                        i.putExtra(Extra.CITIES.getTypeExtra(), cities);
                        i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), attractions);
                        startActivity(i);
                    }else{
                        Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.date_non_corrette), AddInformationPlannedTripActivity.this);
                    }
                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_non_inseriti), AddInformationPlannedTripActivity.this);
                }
            }
        });

        this.findViewById(R.id.fab_add_information).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = FileUtilities.getData(getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());

                if(!titleEditText.getText().toString().isEmpty() && !departureDateEditText.getText().toString().isEmpty()
                    && !returnDateEditText.getText().toString().isEmpty()){
                    final String title = titleEditText.getText().toString();
                    final String departureDate = departureDateEditText.getText().toString();
                    final String returnDate = returnDateEditText.getText().toString();
                    if (!returnDate.equals("") && !title.equals("") && !departureDate.equals("")
                            && DateAndCalendarUtilities.checkDataInput(returnDate)
                            && DateAndCalendarUtilities.checkDataInput(departureDate)
                            && checkDates(departureDate, returnDate)) {
                        addPlannedTrip(title, getCorrectFormatData(departureDate), getCorrectFormatData(returnDate), cities, attractions, email);
                    }else{
                        Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_inseriti_non_corretti), AddInformationPlannedTripActivity.this);
                    }
                }
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.programma_un_viaggio));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addPlannedTrip(final String title, final String departureData, final String returnData,
                                final String cities, final String attractions, final String email){
        final Map<String,String> params = new HashMap<>();
        params.put("type", "addPlannedTrip");
        params.put("title", title);
        params.put("departureDate", departureData);
        params.put("returnDate", returnData);
        if(!cities.equals("[]")){
            try {
                JSONArray jsonArray = new JSONArray(cities);
                params.put("cities", Utility.JSONArrayToList(this, jsonArray).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!attractions.equals("[]")){
            try {
                JSONArray jsonArray = new JSONArray(attractions);
                params.put("attractions",  Utility.JSONArrayToList(this, jsonArray).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        params.put("email", email);

        if (InternetUtilities.getIsNetworkConnected()) {
            AddRequests.addPlannedTrip(this, params);
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

    private String getCorrectFormatData(final String date){
        return date.replace("/",  "-");
    }

    private boolean checkDates(final String departureDateString, final String returnDateString){
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
        try {
            final Date departureDate = dateFormat.parse(departureDateString.replace("-", "/"));
            final Date returnDate = dateFormat.parse(returnDateString.replace("-", "/"));
            if (Objects.requireNonNull(departureDate).compareTo(returnDate) <= 0){
                return true;
            }
        } catch (ParseException e) {
            Log.e(getString(R.string.error), e.toString());
        }
        return false;
    }
}
