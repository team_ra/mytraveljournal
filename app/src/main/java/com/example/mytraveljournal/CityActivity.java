package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Requests.OsmUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;

import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CityActivity extends AppCompatActivity {

    private String longName;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (this.getClass().equals(CityActivity.class)) {
                this.onBackPressed();
                this.finish();
            } else {
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_city);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.cityLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final TextView lon = this.findViewById(R.id.longCityValueTextView);
        final TextView lat = this.findViewById(R.id.latCityValueTextView);
        final TextView cityInfoTextView = this.findViewById(R.id.cityinfoTextView);
        final String type = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        if(type!=null && type.equals("SearchActivity")){
            this.longName = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        }else{
            this.longName = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        }

        final List<String> cityList = new ArrayList<>(Arrays.asList(Objects.requireNonNull(this.longName).split(";")));
        final String cityCountry = cityList.get(cityList.size() - 1);
        final String cityName = cityList.get(0);

        Utility.setUpToolbar(this, cityName);
        cityInfoTextView.setText(this.longName);

        final List<String> list = new ArrayList<>(Collections.singletonList(this.longName));

        String name = list.toString().replace("[","");
        name = name.replace("]","");
        name = name.replace(";",",");

        if (InternetUtilities.getIsNetworkConnected()) {
            OsmUtilities.createCityInfoRequest(this, name, cityName, cityCountry, lat, lon,(TextView)findViewById(R.id.popolCityValueTextView),
                    (TextView)findViewById(R.id.capitalCityValueTextView), (TextView)findViewById(R.id.websiteCityValueTextView), (ImageView)findViewById(R.id.cityImageView));
        } else {
            InternetUtilities.getSnackbar().show();
        }

        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());

        this.findViewById(R.id.addCityToPlannedTripsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(getApplicationContext(), PlannedTripsActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CityActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "city");
                i.putExtra(Extra.NAME.getTypeExtra(), longName);
                startActivity(i);
            }
        });

        this.findViewById(R.id.addDesiredCityButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final String message = getString(R.string.citta_aggiunta_con_successo);
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addDesiredPlace");
                params.put("email", email);
                params.put("name", longName);
                params.put("kind", getString(R.string.city));
                AddRequests.addElement(CityActivity.this, params, message);
            }
        });

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
