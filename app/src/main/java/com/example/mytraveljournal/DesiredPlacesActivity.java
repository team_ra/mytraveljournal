package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ExpandableListAdapter.DesiredPlacesExpandableListAdapter;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.LoadingDialog;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DesiredPlacesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_desiredplaces);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.desiredPlacesLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        final ExpandableListView listView = this.findViewById(R.id.desiredPlacesExpandableListView);
        final DesiredPlacesExpandableListAdapter listAdapter = new DesiredPlacesExpandableListAdapter(DesiredPlacesActivity.this, DesiredPlacesActivity.this);
        listAdapter.setSelectable(true);
        listAdapter.setDeletable(true);
        listView.setAdapter(listAdapter);
        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());
        final HashMap<String, List<String>> values = new HashMap<>();
        final List<String> cities = new ArrayList<>();
        final List<String> attractions = new ArrayList<>();

        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getDesiredPlaces(this, email, cities, attractions, listAdapter, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        if(Objects.requireNonNull(this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra())).equals("AddPlacePlannedTripActivity")){
            final String message;
            final Map<String, String> params = new HashMap<>();
            params.put("type", "addDesiredPlace");
            params.put("email", email);
            params.put("name", this.getIntent().getStringExtra(Extra.NAME.getTypeExtra()));

            if(Objects.requireNonNull(this.getIntent().getStringExtra(Extra.KIND.getTypeExtra())).equals(getString(R.string.city))){
                cities.add(this.getIntent().getStringExtra(Extra.NAME.getTypeExtra()));
                params.put("kind", getString(R.string.city));
                message = getString(R.string.citta_aggiunta_con_successo);
            }else{
                attractions.add(this.getIntent().getStringExtra(Extra.NAME.getTypeExtra()));
                params.put("kind", getString(R.string.attraction));
                message = getString(R.string.attrazione_aggiunta_con_successo);
            }

            if (InternetUtilities.getIsNetworkConnected()) {
                AddRequests.addElement(DesiredPlacesActivity.this, params, message);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        }

        this.findViewById(R.id.desiredPlacesAddCityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddPlacePlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DesiredPlacesActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "city");
                startActivity(i);
            }
        });

        this.findViewById(R.id.desiredPlacesAddAttractionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddPlacePlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DesiredPlacesActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "attraction");
                startActivity(i);
            }
        });

        values.put(this.getString(R.string.citta), cities);
        values.put(this.getString(R.string.attrazioni), attractions);
        listAdapter.setData(values);

        Utility.setUpToolbar(this, this.getString(R.string.luoghi_dei_desideri));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

}
