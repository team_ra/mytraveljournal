package com.example.mytraveljournal;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Requests.OsmUtilities;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class AttractionActivity extends AppCompatActivity {

    private String longName;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (this.getClass().equals(AttractionActivity.class)) {
                this.onBackPressed();
                this.finish();
            } else {
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_attraction);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.attractionLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final TextView attractionInfoTextView = this.findViewById(R.id.infoAttractionTextView);
        final TextView lat = this.findViewById(R.id.latAttrValueTextView);
        final TextView lon = this.findViewById(R.id.lonAttrValueTextView);
        final String type = getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        if(type!=null && type.equals("SearchActivity")){
            this.longName = getIntent().getStringExtra(Extra.ATTRACTION.getTypeExtra());
        }else{
            this.longName = getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        }

        final List<String> attractionList = new ArrayList<>(Arrays.asList(Objects.requireNonNull(this.longName).split(";")));
        final String attractionName = attractionList.get(0);

        Utility.setUpToolbar(this, attractionName);
        attractionInfoTextView.setText(this.longName);

        final List<String> list = new ArrayList<>(Collections.singletonList(this.longName));
        String attrName = list.toString().replace("[","");
        attrName = attrName.replace("]","");
        attrName = attrName.replace(";",",");

        final String email = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());
        if (InternetUtilities.getIsNetworkConnected()) {
            OsmUtilities.createAttractionInfoRequest(this, attrName, attractionName, lat, lon,
                    (TextView)this.findViewById(R.id.websiteAttrValueTextView), (TextView)this.findViewById(R.id.openhoursAttrTextView),
                    (TextView)this.findViewById(R.id.telephoneAttrValueTextView), (ImageView)this.findViewById(R.id.attractionImageView));

        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.addAttrToPlannedTripsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(getApplicationContext(), PlannedTripsActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AttractionActivity");
                i.putExtra(Extra.KIND.getTypeExtra(), "attraction");
                i.putExtra(Extra.NAME.getTypeExtra(), longName);
                startActivity(i);
            }
        });

        this.findViewById(R.id.addDesiredAttrButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final String message = getString(R.string.attrazione_aggiunta_con_successo);
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addDesiredPlace");
                params.put("email", email);
                params.put("name", longName);
                params.put("kind", getString(R.string.attraction));
                if (InternetUtilities.getIsNetworkConnected()) {
                    AddRequests.addElement(AttractionActivity.this, params, message);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
