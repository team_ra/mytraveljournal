package com.example.mytraveljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Adapters.ListAdapter.PlaceDateListAdapter;
import com.example.mytraveljournal.Items.PlaceItem;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.Requests.GetRequests;
import com.example.mytraveljournal.Requests.UpdateRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class OrganizeTripActivity extends AppCompatActivity {

    private List<PlaceItem> placeItemList;
    private PlaceDateListAdapter placeDateListAdapter;
    private String cities;
    private String attractions;
    private TripItem tripItem;

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                placeDateListAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(OrganizeTripActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_organizzare_il_viaggio)
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = createIntent();
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_organizetrip);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.organizeTripLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final ListView listView = this.findViewById(R.id.organizeTripListView);
        final String title = this.getIntent().getStringExtra(Extra.TITLE.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String tripId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        this.cities = this.getIntent().getStringExtra(Extra.CITIES.getTypeExtra());
        this.attractions = this.getIntent().getStringExtra(Extra.ATTRACTIONS.getTypeExtra());
        this.tripItem = new TripItem(tripId , title, "planned", departureDate, returnDate);

        this.placeDateListAdapter = new PlaceDateListAdapter(this, this, tripItem);
        listView.setAdapter(this.placeDateListAdapter);
        this.placeItemList = new ArrayList<>();

        if (InternetUtilities.getIsNetworkConnected()) {
            GetRequests.getPlaceDates(this,tripId, placeItemList,placeDateListAdapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.fab_add_organizetrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String places = placeItemList.toString();
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateOrganizedTrip");
                params.put("places", places);

                final Intent i = createIntent();
                if (InternetUtilities.getIsNetworkConnected()) {
                    UpdateRequests.updateStandard(OrganizeTripActivity.this, params,
                            getString(R.string.viaggio_organizzato_con_successo), i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.organizza_per_data));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private Intent createIntent(){
        final Intent i = new Intent(getApplicationContext(), PlannedTripActivity.class);
        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "OrganizeTripActivity");
        i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
        i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
        i.putExtra(Extra.CITIES.getTypeExtra(), cities);
        i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), attractions);
        return i;
    }
}