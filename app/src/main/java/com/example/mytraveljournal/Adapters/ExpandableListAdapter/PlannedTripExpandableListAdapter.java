package com.example.mytraveljournal.Adapters.ExpandableListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.PlannedPlaceImagesActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Requests.DeleteRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.Utility;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PlannedTripExpandableListAdapter extends ExpandableListAdapter {

    private final Activity activity;
    private final TripItem tripItem;

    public PlannedTripExpandableListAdapter(final Context context, final Activity activity,
                                            final TripItem trip) {
        super(context);
        this.activity = activity;
        this.tripItem = trip;
    }

    @Override
    public void removeElement(final int groupPosition, final  int childPosition){
        final String place =  Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).get(childPosition);
        final Map<String, String> params = new HashMap<>();
        params.put("type", "deletePlace");
        params.put("tripId", tripItem.getTripId());
        params.put("name", place);
        if(super.getHeaders().get(groupPosition).equals(this.activity.getString(R.string.attrazioni))){
            params.put("kind", this.activity.getString(R.string.attraction));
            DeleteRequests.deleteElement(this.activity, params, this.activity.getString(R.string.attrazione_cancellata_con_successo));
        }else{
            params.put("kind", this.activity.getString(R.string.city));
            DeleteRequests.deleteElement(this.activity, params, this.activity.getString(R.string.citta_cancellata_con_successo));
        }
        Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).remove(childPosition);
        this.notifyDataSetChanged();
    }

    @Override
    public void actionOnItem(final int groupPosition, final int childPosition){
        final String value = Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).get(childPosition);
        final Intent i = new Intent(activity.getApplicationContext(), PlannedPlaceImagesActivity.class);
        if(getHeaders().get(groupPosition).equals("Città")){
            i.putExtra(Extra.KIND.getTypeExtra(), "city");
        }else{
            i.putExtra(Extra.KIND.getTypeExtra(), "attraction");
        }
        i.putExtra(Extra.NAME.getTypeExtra(), value);
        i.putExtra(Extra.ID.getTypeExtra(), tripItem.getTripId());
        i.putExtra(Extra.TITLE.getTypeExtra(), tripItem.getName());
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
        final JSONArray cities = Utility.listToJSONArray(activity, Objects.requireNonNull(super.getValues().get("Città")));
        final JSONArray attractions = Utility.listToJSONArray(activity, Objects.requireNonNull(super.getValues().get("Attrazioni")));
        i.putExtra(Extra.CITIES.getTypeExtra(), cities.toString());
        i.putExtra(Extra.ATTRACTIONS.getTypeExtra(), attractions.toString());
        activity.startActivity(i);
    }
}
