package com.example.mytraveljournal.Adapters.ExpandableListAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.mytraveljournal.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final List<String> headers;
    private final HashMap<String,List<String>> values;
    private boolean isSelectable;
    private boolean isDeletable;

    public ExpandableListAdapter(final Context context) {
        this.isSelectable=true;
        this.isDeletable=true;
        this.context=context;
        this.headers = new ArrayList<>();
        this.values = new HashMap<>();
        this.headers.add("Città");
        this.headers.add("Attrazioni");
    }

    public void setSelectable(final boolean selectable) {
        this.isSelectable = selectable;
    }

    public void setDeletable(final boolean deletable) {
        this.isDeletable = deletable;
    }

    public Context getContext() {
        return context;
    }

    List<String> getHeaders() {
        return headers;
    }

    public HashMap<String, List<String>> getValues() {
        return values;
    }

    @Override
    public int getGroupCount() {
        return this.headers.size();
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return this.headers.get(groupPosition);
    }

    @Override
    public Object getChild(final int groupPosition, final int childPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).get(childPosition);
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
        String header = this.headers.get(groupPosition);
        if (convertView == null){
            final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_group, parent, false);
        }
        final TextView lblListHeader = convertView.findViewById(R.id.listHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header);
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String)getChild(groupPosition, childPosition);
        if(convertView == null)
        {
            final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_item, parent, false);
        }

        final TextView txtListChild = convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);

        if(this.isSelectable){
            txtListChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionOnItem(groupPosition, childPosition);
                }
            });
        }

        if(this.isDeletable){
            convertView.findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setTitle(context.getString(R.string.attenzione))
                            .setMessage(R.string.sicuro_di_cancellare)
                            .setCancelable(false)
                            .setPositiveButton(context.getString(R.string.si), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeElement(groupPosition, childPosition);
                                }
                            })
                            .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create();
                    dialog.show();
                }
            });
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return this.isSelectable;
    }

    public void removeElement(final int groupPosition, final int childPosition){ }

    public void actionOnItem(final int groupPosition, final int childPosition){ }

    public void setData(final Map<String, List<String>> values){
        this.values.clear();
        this.values.putAll(values);
        this.notifyDataSetChanged();
    }
}
