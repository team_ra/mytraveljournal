package com.example.mytraveljournal.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Requests.DeleteRequests;
import com.example.mytraveljournal.Utilities.InternetUtilities;

import java.util.HashMap;

public class PlaceImageViewPagerAdapter extends ViewPagerAdapter {

    private final String kind;
    private final String name;
    private final String tripId;

    public PlaceImageViewPagerAdapter(final Context context, final Activity activity,
                                      final String kind, final String name, final String tripId) {
        super(context, activity);
        this.kind = kind;
        this.name = name;
        this.tripId = tripId;
    }

    @Override
    protected void deletePlaceImage(final String element){
        final HashMap<String, String> params = new HashMap<>();
        params.put("type", "deletePlaceImage");
        params.put("kind", this.kind);
        params.put("name", this.name);
        params.put("tripId", this.tripId);
        params.put("image", element);

        if (InternetUtilities.getIsNetworkConnected()) {
            DeleteRequests.deleteElement(getActivity(), params, getActivity().getString(R.string.immagine_cancellata_con_successo));
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

    @Override
    protected int getId(){
            return R.layout.image_plannedtripslider;
    }
}
