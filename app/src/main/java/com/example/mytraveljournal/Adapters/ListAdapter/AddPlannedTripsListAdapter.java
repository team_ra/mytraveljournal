package com.example.mytraveljournal.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddPlannedTripsListAdapter extends PlannedTripsListAdapter {

    private String kind;
    private String name;

    public AddPlannedTripsListAdapter(final Context context, final Activity activity, final String kind, final String name) {
        super(context, activity);
        this.kind = kind;
        this.name = name;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TripItem item = (TripItem) super.getList().get(position);
        final String childText =  item.getName();
        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_itemaddplannedtrips, parent, false);
        }

        final TextView titleTextView = convertView.findViewById(R.id.titleListItemTextView);
        titleTextView.setText(childText);
        final TextView dateTextView = convertView.findViewById(R.id.dateListItemTextView);
        dateTextView.setText(item.getDate());

        convertView.findViewById(R.id.addToTripButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = FileUtilities.getData(getContext().getCacheDir() + "/mtj.txt").get(UserManagement.EMAIL.getId());
                final String departureDate = item.getDepartureDate();
                final String returnDate = item.getReturnDate();
                addPlaceToTrip(email, item.getName(), departureDate, returnDate, kind, name);
            }
        });
        return convertView;
    }

    private void addPlaceToTrip(final String email, final String title,
                             final String departureDate, final String returnDate, final String kind, final String name) {
        final Map<String, String> params = new HashMap<>();
        params.put("type", "addPlaceToTrip");
        params.put("email", email);
        params.put("title", title);
        params.put("departureDate", departureDate);
        params.put("returnDate", returnDate);
        params.put("kind", kind);
        params.put("name", name);

        final String message;
        if(kind.equals(getActivity().getString(R.string.city))){
            message = getActivity().getString(R.string.citta_aggiunta_con_successo);
        }else{
            message = getActivity().getString(R.string.attrazione_aggiunta_con_successo);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            AddRequests.addPlaceToTrip(getActivity(), params, message);
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }
}
