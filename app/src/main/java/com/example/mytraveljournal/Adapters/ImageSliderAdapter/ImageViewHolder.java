package com.example.mytraveljournal.Adapters.ImageSliderAdapter;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytraveljournal.R;

class ImageViewHolder extends RecyclerView.ViewHolder {

    private final ImageView imageView;
    private final ImageButton deleteButton;
    private final ImageButton saveButton;
    private final ImageButton shareButton;

    ImageViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imageView = itemView.findViewById(R.id.imageSliderImageView);
        this.deleteButton = itemView.findViewById(R.id.deletePlaceImageButton);
        this.saveButton = itemView.findViewById(R.id.savePlaceImageButton);
        this.shareButton = itemView.findViewById(R.id.sharePlaceImageButton);
    }

    ImageView getImageView() {
        return this.imageView;
    }

    ImageButton getDeleteButton() {
        return this.deleteButton;
    }

    ImageButton getSaveButton() {
        return this.saveButton;
    }

    ImageButton getShareButton() {
        return this.shareButton;
    }
}
