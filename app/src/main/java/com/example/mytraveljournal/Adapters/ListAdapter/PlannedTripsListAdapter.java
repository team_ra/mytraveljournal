package com.example.mytraveljournal.Adapters.ListAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.PlannedTripActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Requests.AddRequests;
import com.example.mytraveljournal.Requests.DeleteRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlannedTripsListAdapter extends ListAdapter implements Filterable {

    private final List<TripItem> valuesFull;
    private Activity activity;

    public PlannedTripsListAdapter(final Context context, final Activity activity) {
        super(context);
        this.valuesFull = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TripItem item = (TripItem) super.getList().get(position);
        final String childText =  item.getName();
        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_itemplannedtrip, parent, false);
        }

        final TextView titleTextView = convertView.findViewById(R.id.titleListItemTextView);
        titleTextView.setText(childText);
        final TextView dateTextView = convertView.findViewById(R.id.dateListItemTextView);
        dateTextView.setText(item.getDate());

        convertView.findViewById(R.id.addDoneTripButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setMessage(activity.getString(R.string.sicuro_di_non_voler_modificare_questo_viaggio))
                        .setCancelable(false)
                        .setPositiveButton(activity.getString(R.string.si), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String email = FileUtilities.getData(getContext().getCacheDir() + "/mtj.txt").get(UserManagement.EMAIL.getId());
                                addDoneTrip(email, item.getName(), item.getDepartureDate(), item.getReturnDate());
                                getList().remove(item);
                                valuesFull.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }
        });

        convertView.findViewById(R.id.deletePlannedTripButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setMessage(activity.getString(R.string.sicuro_di_voler_cancellare_il_viaggio))
                        .setCancelable(false)
                        .setPositiveButton(activity.getString(R.string.si), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String email = FileUtilities.getData(getContext().getCacheDir() + "/mtj.txt").get(UserManagement.EMAIL.getId());
                                deletePlannedTrip(email, item.getName(), item.getDepartureDate(), item.getReturnDate());
                                getList().remove(item);
                                valuesFull.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }
        });
        convertView.findViewById(R.id.titleListItemTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getContext(), PlannedTripActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlannedTripsActivity");
                i.putExtra(Extra.TITLE.getTypeExtra(), item.getName());
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), item.getDepartureDate());
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), item.getReturnDate());
                i.putExtra(Extra.ID.getTypeExtra(), item.getTripId());
                activity.startActivity(i);
            }
        });
        return convertView;
    }

    public void setData(final List<TripItem> newData) {
        this.valuesFull.clear();
        this.valuesFull.addAll(newData);
        super.setList(newData);
        this.notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return plannedFilter;
    }

    Activity getActivity() {
        return activity;
    }

    private void deletePlannedTrip(final String email, final String title,
                                   final String departureDate, final String returnDate) {
        final Map<String, String> params = new HashMap<>();
        params.put("type", "deletePlannedTrip");
        params.put("email", email);
        params.put("title", title);
        params.put("departureDate", departureDate);
        params.put("returnDate", returnDate);

        if (InternetUtilities.getIsNetworkConnected()) {
            DeleteRequests.deleteElement(activity, params, activity.getString(R.string.viaggio_cancellato_con_successo));
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

    private void addDoneTrip(final String email, final String title,
                             final String departureDate, final String returnDate) {
        final Map<String, String> params = new HashMap<>();
        params.put("type", "addDoneTrip");
        params.put("email", email);
        params.put("title", title);
        params.put("departureDate", departureDate);
        params.put("returnDate", returnDate);

        if (InternetUtilities.getIsNetworkConnected()) {
            AddRequests.addElement(activity, params, activity.getString(R.string.viaggio_aggiunto_con_successo_effettuati));
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

    private Filter plannedFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            List<TripItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(valuesFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (TripItem item : valuesFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
