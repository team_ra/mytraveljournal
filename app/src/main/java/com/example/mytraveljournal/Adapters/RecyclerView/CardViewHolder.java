package com.example.mytraveljournal.Adapters.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytraveljournal.R;

class CardViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

    ImageView imageCardView;
    TextView placeTextView;
    TextView dateTextView;
    ImageView shareButton;
    private OnItemListener itemListener;

    CardViewHolder(@NonNull View itemView, OnItemListener lister) {
        super(itemView);
        imageCardView = itemView.findViewById(R.id.placeImage);
        placeTextView = itemView.findViewById(R.id.placeTextView);
        dateTextView = itemView.findViewById(R.id.dateTextView);
        shareButton = itemView.findViewById(R.id.shareIcon);
        itemListener = lister;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }
}
