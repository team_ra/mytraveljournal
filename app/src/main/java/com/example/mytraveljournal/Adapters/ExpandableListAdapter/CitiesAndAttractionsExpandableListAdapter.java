package com.example.mytraveljournal.Adapters.ExpandableListAdapter;

import android.content.Context;

import java.util.Objects;

public class CitiesAndAttractionsExpandableListAdapter extends ExpandableListAdapter {

    public CitiesAndAttractionsExpandableListAdapter(final Context context) {
        super(context);
    }

    @Override
    public void removeElement(final int groupPosition, final int childPosition){
        Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).remove(childPosition);
        notifyDataSetChanged();
    }
}
