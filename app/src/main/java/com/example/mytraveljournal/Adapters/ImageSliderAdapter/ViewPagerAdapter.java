package com.example.mytraveljournal.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private Context context;
    private List<String> images;
    private final Activity activity;
    private boolean isDeletable;

    ViewPagerAdapter(final Context context, final Activity activity) {
        this.context = context;
        this.isDeletable = false;
        this.images = new ArrayList<>();
        this.activity = activity;
    }

    public Context getContext() {
        return this.context;
    }

    public Activity getActivity() {
        return this.activity;
    }

    public void setDeletable(final boolean deletable) {
        this.isDeletable = deletable;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(context).inflate(this.getId(), parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull ImageViewHolder holder, final int position) {
        final String currentImage = this.images.get(position);
        holder.getImageView().setImageBitmap(ImageUtilities.decodeBitmap(currentImage));

        if(this.isDeletable){
            holder.getDeleteButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog dialog = new AlertDialog.Builder(activity)
                            .setMessage(activity.getString(R.string.sicuro_di_voler_cancellare_immagine))
                            .setCancelable(false)
                            .setPositiveButton(activity.getString(R.string.si), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    deletePlaceImage(images.get(position));
                                    images.remove(position);
                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .create();
                    dialog.show();
                }
            });
        }

        holder.getSaveButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ImageUtilities.saveImage(ImageUtilities.decodeBitmap(images.get(position)), context);
                    Utility.showToast(context.getString(R.string.immagine_salvata_in_galleria), activity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.getShareButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(Intent.createChooser(Utility.shareImage(ImageUtilities.decodeBitmap(images.get(position)), context), "Share With"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setData(final List<String> list){
        this.images.clear();
        this.images.addAll(list);
        this.notifyDataSetChanged();
    }

    public static ViewPager2.OnPageChangeCallback onPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageSelected(final int position) {
            super.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            super.onPageScrollStateChanged(state);
        }
    };

    protected int getId(){
        return -1;
    }

    protected void deletePlaceImage(final String elem){}
}
