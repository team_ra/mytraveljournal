package com.example.mytraveljournal.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.mytraveljournal.Items.PlaceItem;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.DateAndCalendarUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlaceDateListAdapter extends ListAdapter implements Filterable {

    private final List<PlaceItem> placeItemsFull;
    private final Activity activity;
    private final TripItem tripItem;

    public PlaceDateListAdapter(final Context context, final Activity activity, final TripItem tripItem) {
        super(context);
        this.placeItemsFull = new ArrayList<>();
        this.activity = activity;
        this.tripItem = tripItem;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final PlaceItem item = (PlaceItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_itemorganizetrip, parent, false);
        }

        final TextView titleTextView = convertView.findViewById(R.id.titlePlaceTextView);
        titleTextView.setText(item.getName());
        final TextView dateTextView = convertView.findViewById(R.id.datePlaceTextView);
        dateTextView.setText(item.getDate());

        convertView.findViewById(R.id.addDateToPlaceButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateAndCalendarUtilities.showDatePickerDialog(getContext(), dateTextView, item, activity, tripItem);
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return placeFilter;
    }

    public void setData(List<PlaceItem> newData) {
        this.placeItemsFull.clear();
        this.placeItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter placeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<PlaceItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(placeItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final PlaceItem item : placeItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
