package com.example.mytraveljournal.Adapters.ListAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytraveljournal.R;

import java.util.List;
import java.util.Objects;

public class SearchListAdapter extends ListAdapter {

    public SearchListAdapter(Context context){
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final String childText = (String) super.getList().get(position);
        if(view == null) {
            final LayoutInflater inflater = (LayoutInflater)super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.list_itemstandard, viewGroup, false);
        }
        final TextView placeTextView = view.findViewById(R.id.searchListItemTextView);
        placeTextView.setText(childText);

        return view;
    }

    public void setData(final List<String> newData) {
        super.setList(newData);
        this.notifyDataSetChanged();
    }
}
