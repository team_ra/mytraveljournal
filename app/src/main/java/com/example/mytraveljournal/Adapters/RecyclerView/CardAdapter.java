package com.example.mytraveljournal.Adapters.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytraveljournal.Items.CardItem;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.ImageUtilities;
import com.example.mytraveljournal.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> implements Filterable {

    private List<CardItem> cardItemList = new ArrayList<>();
    private List <CardItem> cardItemListFull = new ArrayList<>();
    private Activity activity;

    private OnItemListener listener;

    public CardAdapter(final OnItemListener listener, final Activity activity) {
        this.listener = listener;
        this.activity = activity;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,
                parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        final CardItem currentCardItem = cardItemList.get(position);
        holder.placeTextView.setText(currentCardItem.getPlace());
        holder.dateTextView.setText(currentCardItem.getDate());

        if(currentCardItem.getImageResource().equals("nessuna immagine")){
            final Drawable myDrawable = activity.getResources().getDrawable(R.drawable.logo_immagine);
            holder.imageCardView.setImageDrawable(myDrawable);
        }else{
            final Bitmap bitmap = ImageUtilities.decodeBitmap(currentCardItem.getImageResource());
            holder.imageCardView.setImageBitmap(bitmap);
        }
        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = "Titolo: " + currentCardItem.getPlace() + "\nDate: " + currentCardItem.getDate();
                final String subject = "Il mio viaggio";
                final Intent sendIntent = Utility.shareText(message, subject);

                v.getContext().startActivity(Intent.createChooser(sendIntent, null));
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    private Filter cardFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<CardItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final CardItem item : cardItemListFull) {
                    if (item.getPlace().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();
            final List<?> result = (List<?>) results.values;
            for (final Object object : result) {
                if (object instanceof CardItem) {
                    cardItemList.add((CardItem) object);
                }
            }
            notifyDataSetChanged();
        }
    };

    public void setData(List<CardItem> newData) {
        this.cardItemList.clear();
        this.cardItemList.addAll(newData);
        this.cardItemListFull.clear();
        this.cardItemListFull.addAll(newData);
        this.notifyDataSetChanged();
    }
}