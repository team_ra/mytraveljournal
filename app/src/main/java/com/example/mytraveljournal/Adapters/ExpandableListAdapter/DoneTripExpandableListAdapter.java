package com.example.mytraveljournal.Adapters.ExpandableListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytraveljournal.DonePlaceActivity;
import com.example.mytraveljournal.Items.TripItem;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Utilities.Extra;

import java.util.Objects;

public class DoneTripExpandableListAdapter extends ExpandableListAdapter {

    private final Activity activity;
    private final TripItem tripItem;

    public DoneTripExpandableListAdapter(final Context context, final Activity activity, final TripItem tripItem) {
        super(context);
        this.activity = activity;
        this.tripItem = tripItem;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String)getChild(groupPosition, childPosition);
        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_itemstandard, parent, false);
        }

        TextView txtListChild = convertView.findViewById(R.id.searchListItemTextView);
        txtListChild.setText(childText);

        txtListChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionOnItem(groupPosition, childPosition);
            }
        });

        return convertView;
    }

    @Override
    public void actionOnItem(final int groupPosition, final int childPosition){
        final String value = Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).get(childPosition);
        final Intent i = new Intent(activity.getApplicationContext(), DonePlaceActivity.class);
        if(getHeaders().get(groupPosition).equals("Città")){
            i.putExtra(Extra.KIND.getTypeExtra(), "city");
        }else{
            i.putExtra(Extra.KIND.getTypeExtra(), "attraction");
        }
        i.putExtra(Extra.NAME.getTypeExtra(), value);
        i.putExtra(Extra.ID.getTypeExtra(), this.tripItem.getTripId());
        i.putExtra(Extra.TITLE.getTypeExtra(), this.tripItem.getName());
        i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), this.tripItem.getDepartureDate());
        i.putExtra(Extra.RETURN_DATE.getTypeExtra(), this.tripItem.getReturnDate());
        activity.startActivity(i);
    }
}
