package com.example.mytraveljournal.Adapters.ImageSliderAdapter;


import android.app.Activity;
import android.content.Context;

import com.example.mytraveljournal.R;

public class DonePlaceImageViewPagerAdapter extends ViewPagerAdapter {

    public DonePlaceImageViewPagerAdapter(final Context context, final Activity activity) {
        super(context, activity);
    }

    @Override
    protected int getId(){
        return R.layout.image_donetripslider;
    }
}
