package com.example.mytraveljournal.Adapters.ExpandableListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.example.mytraveljournal.AttractionActivity;
import com.example.mytraveljournal.CityActivity;
import com.example.mytraveljournal.R;
import com.example.mytraveljournal.Requests.DeleteRequests;
import com.example.mytraveljournal.Utilities.Extra;
import com.example.mytraveljournal.Utilities.FileUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.UserManagement;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DesiredPlacesExpandableListAdapter extends ExpandableListAdapter {

    private Activity activity;
    private String kind;
    private String name;

    public DesiredPlacesExpandableListAdapter(final Context context, final Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    public void removeElement(final int groupPosition, final int childPosition){
        final String message;
        this.name = Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).get(childPosition);
        if(super.getHeaders().get(groupPosition).equals(activity.getString(R.string.attrazioni))){
            this.kind = activity.getString(R.string.attraction);
            message = activity.getString(R.string.attrazione_cancellata_con_successo);
        }else{
            this.kind = activity.getString(R.string.city);
            message = activity.getString(R.string.citta_cancellata_con_successo);
        }
        final String email = FileUtilities.getData(super.getContext().getCacheDir()+"/mtj.txt").get(UserManagement.EMAIL.getId());

        final Map<String, String> params = new HashMap<>();
        params.put("type", "deleteDesiredPlace");
        params.put("kind", kind);
        params.put("name", name);
        params.put("email", email);

        if (InternetUtilities.getIsNetworkConnected()) {
            DeleteRequests.deleteElement(activity, params, message);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Objects.requireNonNull(super.getValues().get(super.getHeaders().get(groupPosition))).remove(childPosition);
        notifyDataSetChanged();
    }

    @Override
    public void actionOnItem(final int groupPosition, final int childPosition){
        this.kind = super.getHeaders().get(groupPosition);
        this.name = Objects.requireNonNull(super.getValues().get(kind)).get(childPosition);
        final Intent i;
        if(this.kind.equals(activity.getString(R.string.attrazioni))){
            i = new Intent(activity.getApplicationContext(), AttractionActivity.class);
        }else{
            i = new Intent(activity.getApplicationContext(), CityActivity.class);
        }
        i.putExtra(Extra.TITLE.getTypeExtra(), name);
        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "DesiredPlacesActivity");
        activity.startActivity(i);
    }
}
