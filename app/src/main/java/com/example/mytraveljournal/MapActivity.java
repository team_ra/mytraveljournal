package com.example.mytraveljournal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.mytraveljournal.Utilities.GpsUtilities;
import com.example.mytraveljournal.Utilities.InternetUtilities;
import com.example.mytraveljournal.Utilities.PermissionsUtilities;
import com.example.mytraveljournal.Utilities.Utility;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;

import java.util.Objects;

public class MapActivity extends AppCompatActivity {

    private Bundle savedInstanceState;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                final String lat = intent.getStringExtra("location_latitude");
                final String lon = intent.getStringExtra("location_longitude");
                if (!InternetUtilities.getIsNetworkConnected()) {
                    InternetUtilities.getSnackbar().show();
                } else{
                    setMap(lat, lon);
                }
                GpsUtilities.removeLocationUpdates(MapActivity.this, broadcastReceiver);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtilities.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtilities.createSnackBar(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_map);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.savedInstanceState = savedInstanceState;
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.mapLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));

        if (!PermissionsUtilities.checkPermissions(this)) {
            PermissionsUtilities.requestPermissions(this);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            GpsUtilities.setLocation(this);
            GpsUtilities.requestLocation(this, broadcastReceiver);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, this.getString(R.string.mappe));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void setMap(final String lat, final String lon){
        final SupportMapFragment mapFragment;
        if (this.savedInstanceState == null) {
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            final MapboxMapOptions options = MapboxMapOptions.createFromAttributes(this, null);
            options.camera(new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)))
                    .zoom(9)
                    .build());

            mapFragment = SupportMapFragment.newInstance(options);

            transaction.add(R.id.container, mapFragment, "com.mapbox.map");
            transaction.commit();
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("com.mapbox.map");
        }
        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(@NonNull MapboxMap mapboxMap) {
                    mapboxMap.setStyle(Style.OUTDOORS, new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {
                        }
                    });
                }
            });
        }
    }
}